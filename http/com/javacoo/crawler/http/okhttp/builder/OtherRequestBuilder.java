package com.javacoo.crawler.http.okhttp.builder;

import com.javacoo.crawler.http.okhttp.request.OtherRequest;
import com.javacoo.crawler.http.okhttp.request.RequestCall;

import okhttp3.RequestBody;

/**
 * 其他请求类型构建器
 * <p>
 * 说明:
 * </p>
 * <li></li>
 * 
 * @author DuanYong
 * @since 2018年2月7日下午2:21:09
 */
public class OtherRequestBuilder extends OkHttpRequestBuilder<OtherRequestBuilder> {
	private RequestBody requestBody;
	private String method;
	private String content;

	public OtherRequestBuilder(String method) {
		this.method = method;
	}

	@Override
	public RequestCall build() {
		return new OtherRequest(requestBody, content, method, url, tag, params, headers, id).build();
	}

	public OtherRequestBuilder requestBody(RequestBody requestBody) {
		this.requestBody = requestBody;
		return this;
	}

	public OtherRequestBuilder requestBody(String content) {
		this.content = content;
		return this;
	}
}
