package com.javacoo.crawler.http.okhttp.builder;

import java.io.File;

import com.javacoo.crawler.http.okhttp.request.PostFileRequest;
import com.javacoo.crawler.http.okhttp.request.RequestCall;

import okhttp3.MediaType;
/**
 * 发送文件请求构建器
 * <p>说明:</p>
 * <li></li>
 * @author DuanYong
 * @since 2018年2月7日下午2:22:53
 */
public class PostFileBuilder extends OkHttpRequestBuilder<PostFileBuilder> {
	private File file;
	private MediaType mediaType;

	public OkHttpRequestBuilder file(File file) {
		this.file = file;
		return this;
	}

	public OkHttpRequestBuilder mediaType(MediaType mediaType) {
		this.mediaType = mediaType;
		return this;
	}

	@Override
	public RequestCall build() {
		return new PostFileRequest(url, tag, params, headers, file, mediaType, id).build();
	}
}
