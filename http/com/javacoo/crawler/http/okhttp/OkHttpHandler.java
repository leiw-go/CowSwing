package com.javacoo.crawler.http.okhttp;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.javacoo.crawler.http.HeaderInfo;
import com.javacoo.crawler.http.HttpCallback;
import com.javacoo.crawler.http.HttpConfig;
import com.javacoo.crawler.http.HttpHandler;
import com.javacoo.crawler.http.HttpResponse;
import com.javacoo.crawler.http.okhttp.callback.StringCallback;
import com.javacoo.crawler.http.okhttp.cookie.CookieJarImpl;
import com.javacoo.crawler.http.okhttp.cookie.store.MemoryCookieStore;
import com.javacoo.crawler.http.okhttp.https.Https;
import com.javacoo.crawler.http.util.FastJsonUtil;
import com.javacoo.crawler.http.util.TextUtils;

import okhttp3.Call;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Response;

/**
 * Http请求实现类
 * <p>
 * 说明:
 * </p>
 * <li>基于OkHttp</li>
 * 
 * @author DuanYong
 * @since 2018年2月7日上午10:17:25
 */
public class OkHttpHandler implements HttpHandler {
	protected final Logger logger = LoggerFactory.getLogger(OkHttpHandler.class);
	/** 配置 */
	private HttpConfig httpConfig;

	public OkHttpHandler(HttpConfig httpConfig) {
		this.httpConfig = httpConfig;
		initOkHttp();
	}

	@Override
	public <P> HttpResponse postSync(String address, HeaderInfo headerInfo, P data) {
		String jsonStr = "";
		Long startTime = System.currentTimeMillis();
		try {
			jsonStr = FastJsonUtil.toJSONString(data);
		} catch (Exception e) {
			e.printStackTrace();
		}
		Response response = null;
		try {
			response = OkHttpUtils.postString().url(address).headers(headerInfo.getHeaderMap()).content(jsonStr)
					.mediaType(MediaType.parse("application/json; charset=utf-8")).build().execute();
		} catch (Exception e) {
			logger.error("发送远程服务调用请求时发生异常,请求URL:" + address + ",POST参数:" + jsonStr + ",信息：" + e);
			e.printStackTrace();
		}
		if (logger.isInfoEnabled()) {
			logger.info(address + ",(参数:" + jsonStr + "),请求完成,耗时->" + (System.currentTimeMillis() - startTime) / 1000.0
					+ "秒");
		}
		/**
		 * 读取响应结果
		 */
		try {
			return getResponse(response);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 获取响应结果
	 * <p>
	 * 说明:
	 * </p>
	 * <li></li>
	 * 
	 * @author DuanYong
	 * @param response
	 * @return
	 * @throws IOException
	 * @since 2018年2月7日下午5:37:42
	 */
	private HttpResponse getResponse(Response response) throws IOException {
		HttpResponse httpResponse = new HttpResponse();
		httpResponse.setStatusCode(response.code());
		httpResponse.setContent(response.body().string());
		httpResponse.setContentStream(response.body().byteStream());
		httpResponse.setContentType(response.body().contentType().type());
		httpResponse.setContentLength(response.body().contentLength());
		if(null != response.body().contentType().charset()){
			httpResponse.setCharset(response.body().contentType().charset().name());
		}
		httpResponse.setLastModified(response.header("Last-Modified"));
		return httpResponse;
	}

	@Override
	public HttpResponse getSync(String address, HeaderInfo headerInfo) {
		Long startTime = System.currentTimeMillis();
		Response response = null;
		try {
			response = OkHttpUtils.get().url(address).headers(headerInfo.getHeaderMap()).build().execute();
		} catch (Exception e) {
			logger.error("发送远程服务调用请求时发生异常,请求URL:" + address + ",信息：" + e);
			e.printStackTrace();
		}
		if (logger.isInfoEnabled()) {
			logger.info(address + ",请求完成,耗时->" + (System.currentTimeMillis() - startTime) / 1000.0 + "秒");
		}
		/**
		 * 读取响应结果
		 */
		try {
			return getResponse(response);
		} catch (Exception e) {
             e.printStackTrace();
		}
		return null;
	}
	

	@Override
	public <P> void postAsync(String address, HeaderInfo headerInfo, P data, HttpCallback callback) {
		String jsonStr = "";
		try {
			jsonStr = FastJsonUtil.toJSONString(data);
		} catch (Exception e) {
			e.printStackTrace();
		}
		OkHttpUtils.postString().url(address).headers(headerInfo.getHeaderMap()).content(jsonStr)
				.mediaType(MediaType.parse("application/json; charset=utf-8")).build().execute(new StringCallback() {
					@Override
					public void onError(Call call, Exception e, int id) {
						callback.onError(e);
					}

					@Override
					public void onResponse(String response, int id) {
						HttpResponse httpResponse = new HttpResponse();
						httpResponse.setContent(response);
						callback.onResponse(httpResponse);
					}
				});
	}

	@Override
	public void getAsync(String address, HeaderInfo headerInfo, HttpCallback callback) {
		OkHttpUtils.get().url(address).headers(headerInfo.getHeaderMap()).build().execute(new StringCallback() {
			@Override
			public void onError(Call call, Exception e, int id) {
				callback.onError(e);
			}

			@Override
			public void onResponse(String response, int id) {
				HttpResponse httpResponse = new HttpResponse();
				httpResponse.setContent(response);
				callback.onResponse(httpResponse);
			}
		});
	}

	/**
	 * 初始化okHttp
	 * <p>
	 * 说明:
	 * </p>
	 * <li></li>
	 * 
	 * @author DuanYong
	 * @since 2018年2月7日下午3:38:14
	 */
	private void initOkHttp() {
		CookieJarImpl cookieJar = new CookieJarImpl(new MemoryCookieStore());
		// 设置可访问所有的https网站
		Https.SSLParams sslParams = Https.getSslSocketFactory(null, null, null);
		Proxy proxy = null;
		if(httpConfig.isUseProxy() ){
			if(TextUtils.isNotBlank(httpConfig.getProxyAddress()) && httpConfig.getProxyPort() != 0){
				proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(httpConfig.getProxyAddress(),httpConfig.getProxyPort()));
				logger.info("=====================使用传入的代理========地址："+httpConfig.getProxyAddress()+":"+httpConfig.getProxyPort());
        	}
		}
		OkHttpClient okHttpClient = new OkHttpClient.Builder()
				.connectTimeout(httpConfig.getConnectTimeout(), TimeUnit.MILLISECONDS)// 连接超时(单位:毫秒)
				.writeTimeout(httpConfig.getWriteTimeout(), TimeUnit.MILLISECONDS)// 写入超时(单位:毫秒)
				.readTimeout(httpConfig.getReadTimeout(), TimeUnit.MILLISECONDS)// 读取超时(单位:毫秒)
				// .pingInterval(20, TimeUnit.MILLISECONDS)
				// //websocket轮训间隔(单位:秒)
				// .cache(new Cache(cache.getAbsoluteFile(), cacheSize))//设置缓存
				.cookieJar(cookieJar)// Cookies持久化
				.proxy(proxy)
				.hostnameVerifier(new HostnameVerifier() {
					@Override
					public boolean verify(String hostname, SSLSession session) {
						return true;
					}
				}).sslSocketFactory(sslParams.sSLSocketFactory, sslParams.trustManager)// https配置
				.build();
		OkHttpUtils.initClient(okHttpClient);
	}

	@Override
	public void close() {
	}

}
