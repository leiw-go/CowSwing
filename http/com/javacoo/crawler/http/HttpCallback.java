package com.javacoo.crawler.http;

/**
 * 异步请求回调
 * <p>说明:</p>
 * <li></li>
 * @author DuanYong
 * @since 2018年2月7日下午4:06:41
 */
public interface HttpCallback {
	/**
	 * 异常执行
	 * <p>说明:</p>
	 * <li></li>
	 * @author DuanYong
	 * @param e
	 * @since 2018年2月7日下午4:10:04
	 */
	void onError(Exception e);
   /**
    * 正常返回执行
    * <p>说明:</p>
    * <li></li>
    * @author DuanYong
    * @param response
    * @since 2018年2月7日下午4:10:19
    */
	void onResponse(HttpResponse response);
}
