package com.javacoo.crawler.http;

/**
 * Http处理接口
 * <p>说明:</p>
 * <li></li>
 * @author DuanYong
 * @since 2018年2月7日下午1:04:29
 */
public interface HttpHandler {
	/**
	 * 异步post请求
	 * <p>说明:</p>
	 * <li></li>
	 * @author DuanYong
     * @param address 请求地址
     * @param headerInfo 头信息
     * @param data 请求数据
     * @param callback 回调实现类
	 * @since 2018年2月7日下午1:06:06
	 */
	<P> void postAsync(String address,HeaderInfo headerInfo,P data, HttpCallback callback);
	/**
	 *  异步get请求
	 * <p>说明:</p>
	 * <li></li>
	 * @author DuanYong
     * @param address 请求地址
     * @param headerInfo 头信息
     * @param callback 回调实现类
	 * @since 2018年2月7日下午1:07:12
	 */
	void getAsync(String address,HeaderInfo headerInfo, HttpCallback callback);
	/**
	 * 同步post请求
	 * <p>说明:</p>
	 * <li></li>
	 * @author DuanYong
     * @param address 请求地址
     * @param headerInfo 头信息
     * @param data 请求数据
	 * @return 响应数据
	 * @since 2018年2月7日下午1:06:06
	 */
	<P> HttpResponse postSync(String address,HeaderInfo headerInfo,P data);
	/**
	 *  同步get请求
	 * <p>说明:</p>
	 * <li></li>
	 * @author DuanYong
     * @param address 请求地址
     * @param headerInfo 头信息
	 * @return 响应数据
	 * @since 2018年2月7日下午1:07:12
	 */
	HttpResponse getSync(String address,HeaderInfo headerInfo);
	/**
	 * 关闭httpHandler
	 * <p>说明:</p>
	 * <li></li>
	 * @since 2018年2月8日下午4:09:54
	 */
	void close();
}
