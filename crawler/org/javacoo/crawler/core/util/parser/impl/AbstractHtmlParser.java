package org.javacoo.crawler.core.util.parser.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.lucene.queryparser.xml.ParserException;
import org.javacoo.crawler.core.constants.Constants;
import org.javacoo.crawler.core.util.CommonUtils;
import org.javacoo.crawler.core.util.parser.HtmlParser;
/**
 * 
 * HTML解析工具类接口抽象实现类
 * <p>
 * 说明:
 * </p>
 * <li></li>
 * @author DuanYong
 * @since 2015-11-30上午10:14:16
 * @version 1.0
 */
public abstract class AbstractHtmlParser implements HtmlParser{
	/**
	 * 内容处理
	 * <p>
	 * 说明:
	 * </p>
	 * <li></li>
	 * @author DuanYong
	 * @since 2015-11-30上午10:15:42
	 * @version 1.0
	 * @param orginHtml 原始内容
	 * @param contentHandleMap 内容处理参数MAP
	 * @return 处理后结果
	 */
	protected String contentHandle(String orginHtml,Map<String, String> contentHandleMap) {
		String returnStr = orginHtml;
		if(StringUtils.isNotBlank(returnStr) && null != contentHandleMap && !contentHandleMap.isEmpty()){
			//如果包含了分隔符
			if(contentHandleMap.containsKey(Constants.CONTENT_FILTER_KEY)){
				String filter = contentHandleMap.get(Constants.CONTENT_FILTER_KEY);
				if(StringUtils.isNotBlank(filter)){
					if(filter.contains("|")){
						String[] filterArr = filter.split("|");
					    for(String tempFilter : filterArr){
					    	returnStr = returnStr.replaceAll(tempFilter, "");
					    }
					}else{
						returnStr = returnStr.replaceAll(filter, "");
					}
				}
			}
			//如果包含了分隔符
			if(contentHandleMap.containsKey(Constants.CONTENT_SEPARATOR_KEY)){
				int posInt = 0;
				String sep = contentHandleMap.get(Constants.CONTENT_SEPARATOR_KEY);
				String[] tempArr =  returnStr.split(sep);
				//如果有位置
				if(contentHandleMap.containsKey(Constants.CONTENT_POSITION_KEY)){
					String pos = contentHandleMap.get(Constants.CONTENT_POSITION_KEY);
					if(NumberUtils.isNumber(pos)){
						posInt = Integer.valueOf(pos);
					}
				}
				if(tempArr.length > posInt){
					returnStr = tempArr[posInt];
				}else{
					returnStr = tempArr[0];
				}
			}
		}
		return returnStr;
	}
	
	protected List<Map<String,String>> parserParams(String params){
		List<Map<String,String>> paramMapList = new ArrayList<Map<String,String>>();
		if(StringUtils.isNotBlank(params)){
			if(params.contains(Constants.RULE_CHILD_KEY)){
				String[] paramArr = params.split(Constants.RULE_CHILD_KEY);
				for(String tempParam : paramArr){
					parserParam(tempParam, paramMapList);
				}
			}else{
				String posStr = "";
				if(params.contains(Constants.RULE_BEGIN_POS_KEY)){
					posStr = CommonUtils.getRegexString(params,Constants.RULE_POS_REGEX);
					if(StringUtils.isNotBlank(posStr)){
						params = params.replace(posStr, "");
					}
					posStr = posStr.replace(Constants.RULE_BEGIN_POS_KEY, "").replace(Constants.RULE_END_POS_KEY, "");
				}
				Map<String,String> paramMap = new HashMap<String,String>();
				paramMap.put(params, posStr);
				paramMapList.add(paramMap);
			}
		}
		return paramMapList;
	}

	private void parserParam(String tempParam,List<Map<String,String>> paramMapList) {
		if(StringUtils.isNotBlank(tempParam)){
			String posStr = "";
			if(tempParam.contains(Constants.RULE_BEGIN_POS_KEY)){
				posStr = CommonUtils.getRegexString(tempParam,Constants.RULE_POS_REGEX);
				if(StringUtils.isNotBlank(posStr)){
					tempParam = tempParam.replace(posStr, "");
				}
				posStr = posStr.replace(Constants.RULE_BEGIN_POS_KEY, "").replace(Constants.RULE_END_POS_KEY, "");
			}
			Map<String,String> paramMap = new HashMap<String,String>();
			paramMap.put(tempParam, posStr);
			paramMapList.add(paramMap);
		}
	}
	
	/**
	 * 根据参数MAP组装NodeFilter集合
	 * @param map  参数MAP
	 * @return NodeFilter集合
	 * @throws ParserException 
	 */
	protected List<Map<String,String[]>> populateFilter(Map<String, String> map){
		List<Map<String,String[]>> nodeFilterList = new CopyOnWriteArrayList<Map<String,String[]>>();
		String tempKey = null;
		String tempValue = null;
		String[] tempValueArr = null;
		Map<String,String[]> filterMap = null;
		for(Iterator<String> it = map.keySet().iterator(); it.hasNext();){
			tempKey = it.next();
			tempValue = map.get(tempKey);
			filterMap = new HashMap<String,String[]>();
			if(tempValue.contains("|")){
				tempValueArr = tempValue.split("\\|");
			}else{
				tempValueArr = new String[]{tempValue};
			}
			filterMap.put(tempKey, tempValueArr);
			nodeFilterList.add(filterMap);
		}
		return nodeFilterList;
	}

}
