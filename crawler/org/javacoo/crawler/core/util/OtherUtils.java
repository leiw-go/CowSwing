package org.javacoo.crawler.core.util;

import java.net.URI;
import java.net.URISyntaxException;

import org.javacoo.crawler.core.data.Task;


/**
 * 一些工具方法
 * @author DuanYong
 *
 */
public class OtherUtils {
	/**
	 * 计算用时
	 * @param timeInSeconds
	 * @return
	 */
	public static String getUseTime(long timeInSeconds) {
		long days, hours, minutes, seconds;
		// 1(day)*24(hour)*60(minite)*60(seconds)*1000(millisecond)
		days = timeInSeconds / 86400000;
		timeInSeconds = timeInSeconds - (days * 3600 * 24 * 1000);
		// 1(hour)*60(minite)*60(seconds)*1000(millisecond)
		hours = timeInSeconds / 3600000;
		timeInSeconds = timeInSeconds - (hours * 3600 * 1000);
		// 1(seconds)*1000(millisecond)
		minutes = timeInSeconds / 60000;
		timeInSeconds = timeInSeconds - (minutes * 60 * 1000);
		// 1(seconds)*1000(millisecond)
		seconds = timeInSeconds / 1000;
		return days + "天" + hours + "小时" + minutes + "分" + seconds
				+ "秒";
	}
	/**
	 * URL host检查
	 * <li>默认返回TRUE，当只爬取本站URL时才检查</li>
	 * @author DuanYong 
	 * @since 2015-1-22上午9:39:38
	 * @version 1.0
	 * @param task
	 * @return
	 */
	public static boolean checkHost(Task task){
		//如果只爬取本站URL，且当前主机不为空，且
		if(task.getController().getCrawlScope().isOnlyDefaultHost()){
			if(null == task.getCrawlURI().getHost() || 
					!task.getCrawlURI().getHost().contains(task.getController().getCrawlScope().getDefaultHostSuffix())){
				return false;
			}
        }
		return true;
	}
	public static void main(String[] args){
		String tempUrl = "http://home.meishichina.com/recipe/recai/page/2/";
		URI uri = null;
		try {
			uri = new URI(tempUrl);
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String host = uri.getHost();
		host = host.substring(host.indexOf("."), host.length());
		System.out.println("www.meishichina.com".contains(host));
	}
}
