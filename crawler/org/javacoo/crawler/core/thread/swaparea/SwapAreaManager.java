package org.javacoo.crawler.core.thread.swaparea;

/**
 * 数据交换区管理接口
 * <p>说明:</p>
 * <li></li>
 * @auther DuanYong
 * @since 2016年5月15日下午7:16:25
 */
public interface SwapAreaManager {
	
	/**
	 * 
	 * 为当前请求构建并初始化一个新的数据交换区实例。
	 * <p>
	 * @return SwapArea 创建的数据交换区实例
	 */		
	SwapArea buildNewSwapArea();
	
	/**
	 * 
	 * 获取当前请求的数据交换区实例。
	 * <p>
	 * @return SwapArea 当前请求绑定的数据交换区实例
	 */		
	SwapArea getCurrentSwapArea();
	
	
	/**
	 * 
	 * 释放当前请求的数据交换区实例。
	 * <p>
	 * @return SwapArea 当前请求绑定的数据交换区实例
	 */	
	SwapArea releaseCurrentSwapArea();

}
