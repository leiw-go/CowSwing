package org.javacoo.crawler.core.data.queue;

import java.util.HashSet;
import java.util.Set;

import org.javacoo.crawler.core.data.uri.CrawlLinkURI;





/**
 * URL队列
 * @author javacoo
 * @since 2012-04-20
 */
public class UrlQueue implements TaskQueue<CrawlLinkURI>{
	/**URL集合*/
	private Set<CrawlLinkURI> allUrl = new HashSet<CrawlLinkURI>();
	/**已访问URL集合*/
	private Set<CrawlLinkURI> visitedUrl = new HashSet<CrawlLinkURI>();
	/**待访问URL集合*/
	private Queue<CrawlLinkURI> unVisitedUrl = new BlockingQueue<CrawlLinkURI>();

	/**
	 * 判断是否为空
	 * @return
	 */
	@Override
	public boolean isEmpty(){
		return unVisitedUrl.isEmpty();
	}
	/**
	 * 清空URL队列
	 */
    @Override
	public void clear(){
		unVisitedUrl.clear();
		visitedUrl.clear();
	}
    @Override
	public void addExecTask(CrawlLinkURI t) {
		visitedUrl.add(t);
	}
	public void addAllURI(CrawlLinkURI t){
		allUrl.add(t);
	}
    @Override
	public boolean addUnExecTask(CrawlLinkURI t) {
		if (!allUrl.contains(t)){
			allUrl.add(t);
			unVisitedUrl.enQueue(t);
			return true;
		}
		return false;
	}
    @Override
	public int getExecTaskNum() {
		return visitedUrl.size();
	}
    @Override
	public Queue<CrawlLinkURI> getUnExecTask() {
		return unVisitedUrl;
	}
    @Override
	public int getUnExecTaskNum() {
		return unVisitedUrl.getSize();
	}
    @Override
	public void removeExecTask(CrawlLinkURI t) {
		visitedUrl.remove(t);
	}
    @Override
	public CrawlLinkURI unExecTaskDeQueue() {
		return unVisitedUrl.deQueue();
	}
}
