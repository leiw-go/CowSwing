package org.javacoo.crawler.core.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.javacoo.crawler.core.data.uri.CrawlResURI;


/**
 * 采集内容值对象
 * @author javacoo
 * @since 2011-11-8
 */
public class ContentBean {
	/**采集任务ID*/
	private Integer acquId;
	/**编码*/
	private String charset;
	/**标题*/
	private String title;
	/**文章内容*/
	private String content;
	/**文章无HTML格式内容*/
	private String contentPlainText;
	/**原始HTML内容*/
	private String orginHtml;
	/**原始HTML内容*/
	private List<String> orginHtmlList;
	/**分页内容集合*/
	private List<String> contentList = new ArrayList<String>();
	/**评论内容集合*/
	private List<String> commentList = new ArrayList<String>();
	/**摘要*/
	private String brief;
	/**标题图片*/
	private String titleImg;
	/**url*/
	private String url;
	/**关键字*/
	private String keywords;
	/**资源URI集合*/
	private List<CrawlResURI> resCrawlURIList = new ArrayList<CrawlResURI>();
	/**字段对应值Map*/
	private Map<String,String> fieldValueMap = new HashMap<String, String>();
	/**组装后的扩展字段集合*/
	List<CrawlerExtendFieldBean> extendFieldList = new ArrayList<CrawlerExtendFieldBean>();
	/**字段对应值Map集合*/
	private List<Map<String,String>> fieldValueMapList = new ArrayList<>();
	public ContentBean() {
		super();
	}
	public ContentBean(String title, String content, Integer acquId) {
		super();
		this.title = title;
		this.content = content;
		this.acquId = acquId;
	}
	
	public String getCharset() {
		return charset;
	}
	public void setCharset(String charset) {
		this.charset = charset;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
	public String getContentPlainText() {
		return contentPlainText;
	}
	public void setContentPlainText(String contentPlainText) {
		this.contentPlainText = contentPlainText;
	}
	public String getOrginHtml() {
		return orginHtml;
	}
	public void setOrginHtml(String orginHtml) {
		this.orginHtml = orginHtml;
	}
	public List<String> getOrginHtmlList() {
		return orginHtmlList;
	}
	public void setOrginHtmlList(List<String> orginHtmlList) {
		this.orginHtmlList = orginHtmlList;
	}
	public Integer getAcquId() {
		return acquId;
	}
	public void setAcquId(Integer acquId) {
		this.acquId = acquId;
	}
	public String getBrief() {
		return brief;
	}
	public void setBrief(String brief) {
		this.brief = brief;
	}
	public String getTitleImg() {
		return titleImg;
	}
	public void setTitleImg(String titleImg) {
		this.titleImg = titleImg;
	}
	
	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}
	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	
	public String getKeywords() {
		return keywords;
	}
	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}
	public Map<String, String> getFieldValueMap() {
		return fieldValueMap;
	}
	public void setFieldValueMap(Map<String, String> fieldValueMap) {
		this.fieldValueMap = fieldValueMap;
	}
	public List<String> getContentList() {
		return contentList;
	}
	public void setContentList(List<String> contentList) {
		this.contentList = contentList;
	}
	
	public List<String> getCommentList() {
		return commentList;
	}
	public void setCommentList(List<String> commentList) {
		this.commentList = commentList;
	}
	
	
	public List<CrawlResURI> getResCrawlURIList() {
		return resCrawlURIList;
	}
	public void setResCrawlURIList(List<CrawlResURI> resCrawlURIList) {
		this.resCrawlURIList = resCrawlURIList;
	}
	
	public List<CrawlerExtendFieldBean> getExtendFieldList() {
		return extendFieldList;
	}
	public void setExtendFieldList(List<CrawlerExtendFieldBean> extendFieldList) {
		this.extendFieldList = extendFieldList;
	}
	
	public List<Map<String, String>> getFieldValueMapList() {
		return fieldValueMapList;
	}
	public void setFieldValueMapList(List<Map<String, String>> fieldValueMapList) {
		this.fieldValueMapList = fieldValueMapList;
	}
	/**
	 * 清理方法，主要是清理采集回来的内容
	 */
	public void clear(){
		setContent(null);
		setOrginHtml(null);
		setBrief(null);
		setTitleImg(null);
		if(null != this.fieldValueMap){
			this.fieldValueMap.clear();
		}
		setFieldValueMap(null);
		if(null != this.contentList){
			this.contentList.clear();
		}
		setContentList(null);
		if(null != this.commentList){
			this.commentList.clear();
		}
		setCommentList(null);
		if(null != this.resCrawlURIList){
			this.resCrawlURIList.clear();
		}
		setResCrawlURIList(null);
		
		setExtendFieldList(null);
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContentBean other = (ContentBean) obj;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		if (url == null) {
			if (other.url != null)
				return false;
		} else if (!url.equals(other.url))
			return false;
		return true;
	}
	
	

}
