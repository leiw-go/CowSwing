package org.javacoo.crawler.core.data;

import java.io.Serializable;

/**
 * URL
 * @author javacoo
 * @since 2012-04-21
 */
public class Url implements Serializable{
	private static final long serialVersionUID = 1L;
	/**URL*/
	private String url;
	/**标题*/
	private String title;
	/**类型*/
	private String type;
	/**路径类型，绝对:0,相对根路径:1,相对当前路径:2*/
	private String pathType;
	public Url() {
		super();
	}
	public Url(String url,String title, String type,String pathType) {
		super();
		this.url = url;
		this.title = title;
		this.type = type;
		this.pathType = pathType;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	public String getPathType() {
		return pathType;
	}
	public void setPathType(String pathType) {
		this.pathType = pathType;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((url == null) ? 0 : url.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Url other = (Url) obj;
		if (url == null) {
			if (other.url != null)
				return false;
		} else if (!url.equals(other.url))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Url [title=" + title + ", type=" + type + ", url=" + url + "]";
	}
	
	
	

}
