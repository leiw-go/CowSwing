/**
 * 如石子一粒,仰高山之巍峨,但不自惭形秽.
 * 若小草一棵,慕白杨之伟岸,却不妄自菲薄.
 */
package org.javacoo.cowswing.plugin.scheduler.service;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.javacoo.cowswing.base.service.AbstractCrawlerService;
import org.javacoo.cowswing.core.constant.Constant;
import org.javacoo.cowswing.core.dispose.DisposeService;
import org.javacoo.cowswing.core.event.CowSwingEvent;
import org.javacoo.cowswing.core.event.CowSwingEventType;
import org.javacoo.cowswing.plugin.scheduler.constant.SchedulerConstant;
import org.javacoo.cowswing.plugin.scheduler.domain.Scheduler;
import org.javacoo.cowswing.plugin.scheduler.service.beans.ScheduleTaskBean;
import org.javacoo.cowswing.plugin.scheduler.service.beans.SchedulerCriteria;
import org.javacoo.persistence.PaginationSupport;
import org.springframework.stereotype.Component;

/**
 * 定时任务服务类
 * <p>说明:</p>
 * <li></li>
 * @author DuanYong
 * @since 2013-5-5 上午11:50:18
 * @version 1.0
 */
@Component("crawlerSchedulerService")
public class CrawlerSchedulerServiceImpl extends AbstractCrawlerService<ScheduleTaskBean,Scheduler,SchedulerCriteria> implements DisposeService{

	/* (non-Javadoc)
	 * @see org.javacoo.crawler.service.AbstractCrawlerService#doInsert(org.javacoo.crawler.service.beans.CrawlerBaseBean, java.lang.String)
	 */
	@Override
	protected int doInsert(ScheduleTaskBean t, String sqlMapId)
			throws IllegalAccessException, InvocationTargetException {
		Scheduler scheduling = new Scheduler();
		BeanUtils.copyProperties(scheduling, t);
		this.getPersistService().insertBySqlMap(sqlMapId, scheduling);
		return scheduling.getSchedulerId();
	}

	/* (non-Javadoc)
	 * @see org.javacoo.crawler.service.AbstractCrawlerService#doUpdate(org.javacoo.crawler.service.beans.CrawlerBaseBean, java.lang.String)
	 */
	@Override
	protected int doUpdate(ScheduleTaskBean t, String sqlMapId)
			throws IllegalAccessException, InvocationTargetException {
		Scheduler scheduling = new Scheduler();
		BeanUtils.copyProperties(scheduling, t);
		return this.getPersistService().updateBySqlMap(sqlMapId, scheduling);
	}

	/* (non-Javadoc)
	 * @see org.javacoo.crawler.service.AbstractCrawlerService#doDelete(org.javacoo.crawler.service.beans.CrawlerBaseBean, java.lang.String)
	 */
	@Override
	protected int doDelete(ScheduleTaskBean t, String sqlMapId)
			throws IllegalAccessException, InvocationTargetException {
		Scheduler scheduling = new Scheduler();
		BeanUtils.copyProperties(scheduling, t);
		return this.getPersistService().deleteBySqlMap(sqlMapId, scheduling);
	}

	/* (non-Javadoc)
	 * @see org.javacoo.crawler.service.AbstractCrawlerService#doGet(org.javacoo.crawler.service.beans.CrawlerBaseBean, java.lang.String)
	 */
	@Override
	protected Scheduler doGet(ScheduleTaskBean t, String sqlMapId)
			throws IllegalAccessException, InvocationTargetException {
		Map<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("schedulerId", t.getSchedulerId());
		return (Scheduler) this.getPersistService().findObjectBySqlMap(sqlMapId, paramMap);
	}

	/* (non-Javadoc)
	 * @see org.javacoo.crawler.service.AbstractCrawlerService#doGetList(java.lang.Object, java.lang.String)
	 */
	@Override
	protected List<Scheduler> doGetList(SchedulerCriteria q, String sqlMapId)
			throws IllegalAccessException, InvocationTargetException {
		return (List<Scheduler>) this.getPersistService().findListBySqlMap(sqlMapId, q);
	}

	/* (non-Javadoc)
	 * @see org.javacoo.crawler.service.AbstractCrawlerService#doGetPaginatedList(java.lang.Object, java.lang.String)
	 */
	@Override
	protected PaginationSupport<Scheduler> doGetPaginatedList(
			SchedulerCriteria q, String sqlMapId)
			throws IllegalAccessException, InvocationTargetException {
		return (PaginationSupport<Scheduler>) this.getPersistService().findPaginatedListBySqlMap(sqlMapId, q, q.getStartIndex(), q.getPageSize());
	}

	/* (non-Javadoc)
	 * @see org.javacoo.crawler.service.AbstractCrawlerService#translateBean(java.lang.Object)
	 */
	@Override
	protected ScheduleTaskBean translateBean(Scheduler e)
			throws IllegalAccessException, InvocationTargetException {
		ScheduleTaskBean schedulingBean = new ScheduleTaskBean();
		BeanUtils.copyProperties(schedulingBean, e);
		return schedulingBean;
	}

	@Override
	public void dispose() {
		try {
			List<Scheduler> result = doGetList(null,SchedulerConstant.SQLMAP_ID_LIST_CRAWLER_SCHEDULING);
			ScheduleTaskBean tempSchedulerBean = null;
			for(Scheduler s : result){
				if(Constant.TASK_STATUS_STOP.equals(s.getStatus())){
					continue;
				}
				tempSchedulerBean = new ScheduleTaskBean();
				tempSchedulerBean.setSchedulerId(s.getSchedulerId());
				tempSchedulerBean.setStatus(Constant.TASK_STATUS_STOP);
				tempSchedulerBean.setEndTime(new Date());
				tempSchedulerBean.setCowSwingEvent(new CowSwingEvent(this,CowSwingEventType.SchedulerTableStopEvent));
				this.update(tempSchedulerBean,SchedulerConstant.SQLMAP_ID_UPDATE_CRAWLER_SCHEDULING);
			}
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
