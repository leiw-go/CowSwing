package org.javacoo.cowswing.plugin.gather.ui.view.panel.rule;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.javacoo.cowswing.core.loader.LanguageLoader;
import org.javacoo.cowswing.plugin.gather.service.beans.RuleContentPageBean;
import org.javacoo.cowswing.ui.view.panel.AbstractContentPanel;
import org.javacoo.cowswing.ui.view.panel.GBC;
import org.springframework.stereotype.Component;
/**
 * 内容分页属性采集规则设置
 *@author DuanYong
 *@since 2012-11-10上午9:55:07
 *@version 1.0
 */
@Component("ruleContentPageSettingPanel")
public class RuleContentPageSettingPanel extends AbstractContentPanel<RuleContentPageBean>{
	private static final long serialVersionUID = 1L;
	/**内容分页属性输入框*/
	private javax.swing.JTextArea contentPageArea;
	/**内容分页属性标签*/
	private javax.swing.JLabel contentPageLabel;
	/**内容分页过滤属性输入框*/
	private javax.swing.JTextArea contentPageFilterArea;
	

	/**
	 * 初始化面板控件
	 */
	protected void initComponents() {
		useGridBagLayout();
		contentPageLabel = new javax.swing.JLabel();
		contentPageArea = new javax.swing.JTextArea(5, 20);
		contentPageFilterArea = new javax.swing.JTextArea(5, 20);
		
		// ====================页面组件设置==================
		
		JLabel contentLabel = new javax.swing.JLabel(LanguageLoader.getString("RuleContentSetting.content"));
		JLabel filterLabel = new javax.swing.JLabel(LanguageLoader.getString("RuleContentSetting.filter"));

		// 添加标签
		addCmpToGridBag(contentLabel,new GBC(1,0));
		addCmpToGridBag(filterLabel,new GBC(2,0));
        //第二行
		contentPageLabel.setText(LanguageLoader.getString("RuleContentSetting.contentPage"));
		addCmpToGridBag(contentPageLabel,new GBC(0,1));

		JPanel contentPageAreaPanel = new JPanel(new BorderLayout());
		contentPageArea.setLineWrap(true);// 激活自动换行功能
		contentPageArea.setWrapStyleWord(true);// 激活断行不断字功能
		contentPageAreaPanel.add(new JScrollPane(contentPageArea));
		addCmpToGridBag(contentPageAreaPanel,new GBC(1,1));

		JPanel contentPageFilterAreaPanel = new JPanel(new BorderLayout());
		contentPageFilterArea.setLineWrap(true);
		contentPageFilterArea.setWrapStyleWord(true);// 激活断行不断字功能
		contentPageFilterAreaPanel.add(new JScrollPane(contentPageFilterArea));
		addCmpToGridBag(contentPageFilterAreaPanel,new GBC(2,1));
		addCmpToGridBag(new JLabel(),new GBC(0,3,3,1));
	}
	
	protected void fillComponentData(RuleContentPageBean ruleContentPageBean){
		contentPageArea.setText(ruleContentPageBean.getPaginationStart());
		contentPageFilterArea.setText(ruleContentPageBean.getPaginationEnd());
	}
	
	public void initData(RuleContentPageBean t){
		if(null == t){
			t = new RuleContentPageBean();
		}
		fillComponentData(t);
	}
	
	@Override
	protected RuleContentPageBean populateData() {
		RuleContentPageBean ruleContentPageBean = new RuleContentPageBean();
		ruleContentPageBean.setPaginationStart(contentPageArea.getText());
		ruleContentPageBean.setPaginationEnd(contentPageFilterArea.getText());
		return ruleContentPageBean;
	}
	

}
