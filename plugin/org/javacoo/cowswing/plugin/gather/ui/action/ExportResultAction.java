package org.javacoo.cowswing.plugin.gather.ui.action;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.javacoo.cowswing.base.service.ICrawlerService;
import org.javacoo.cowswing.core.constant.Constant;
import org.javacoo.cowswing.core.loader.ImageLoader;
import org.javacoo.cowswing.core.loader.LanguageLoader;
import org.javacoo.cowswing.core.utils.DateUtil;
import org.javacoo.cowswing.core.utils.JsonUtils;
import org.javacoo.cowswing.main.CowSwingMainFrame;
import org.javacoo.cowswing.plugin.gather.constant.GatherConstant;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerHistoryBean;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerHistoryCriteria;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerRuleBean;
import org.javacoo.cowswing.plugin.gather.ui.model.CrawlerRuleTabelModel;
import org.javacoo.cowswing.plugin.gather.ui.view.panel.ScanRuleListPage;
import org.javacoo.cowswing.ui.view.dialog.WaitingDialog;
import org.javacoo.persistence.PaginationSupport;
import org.springframework.stereotype.Component;

/**
 * 导出任务结果
 * 
 * <p>说明:</p>
 * <li></li>
 * @author DuanYong
 * @since 2015-3-4 上午10:01:55
 * @version 1.0
 */
@Component("exportResultAction")
public class ExportResultAction extends AbstractAction{
	protected Logger logger = Logger.getLogger(ExportResultAction.class);
	private static final long serialVersionUID = 1L;
	private JTable ruleTable;
	
	private CrawlerRuleTabelModel crawlerRuleTabelModel;
	
	@Resource(name="scanRuleListPage")
	private ScanRuleListPage ruleListPage;

	@Resource(name="cowSwingMainFrame")
	private CowSwingMainFrame crawlerMainFrame;
	
	/**采集历史*/
	@Resource(name="crawlerHistoryService")
	private ICrawlerService<CrawlerHistoryBean,CrawlerHistoryCriteria> crawlerHistoryService;
	
	
	public ExportResultAction(){
		super(LanguageLoader.getString("Scan.export"),ImageLoader.getImageIcon("CrawlerResource.download"));
		
		this.setEnabled(false);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		ruleTable = ruleListPage.getCrawlerRuleTable();
		if(ruleTable.getSelectedRows().length > 0){
			crawlerRuleTabelModel = (CrawlerRuleTabelModel)ruleTable.getModel();
			CrawlerRuleBean tempCrawlerRuleBean = crawlerRuleTabelModel.getRowObject(ruleTable.getSelectedRows()[0]);
			CrawlerHistoryCriteria q = new CrawlerHistoryCriteria();
			q.setRuleId(tempCrawlerRuleBean.getRuleId());
			PaginationSupport<CrawlerHistoryBean> historyResult = crawlerHistoryService.getPaginatedList(q, GatherConstant.SQLMAP_ID_GET_LIST_CRAWLER_CONTENT_HISTORY);
			if(CollectionUtils.isEmpty(historyResult.getData())){
				JOptionPane.showMessageDialog(crawlerMainFrame,LanguageLoader.getString("Scan.exportDataIsBlank"),
						 LanguageLoader.getString("Common.alertTitle"),
						 JOptionPane.CLOSED_OPTION);
				return;
			}
			int result = JOptionPane.showConfirmDialog(null, LanguageLoader.getString("Scan.exportConfirm")+historyResult.getTotalCount(),LanguageLoader.getString("RuleList.confirm"), JOptionPane.YES_NO_OPTION); 
			if(result == 0){
				JFileChooser jfc = new JFileChooser();
				jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				jfc.setDialogTitle(LanguageLoader.getString("Scan.exportSelectDir"));
				int pathResult = jfc.showOpenDialog(null);
				if (pathResult == 1) {
					return; // 撤销则返回
				} else {
					crawlerRuleTabelModel = (CrawlerRuleTabelModel)ruleTable.getModel();
					File f = jfc.getSelectedFile();// f为选择到的目录
					String importRuleName = "ScanResultList"+DateUtil.dateToStrYYYMMDD(new Date());
					if(ruleTable.getSelectedRows().length == 1){
						importRuleName = crawlerRuleTabelModel.getRowObject(ruleTable.getSelectedRow()).getRuleName();
					}else{
						importRuleName = JOptionPane.showInputDialog(LanguageLoader.getString("Scan.exportInput")); 
					}
					if(StringUtils.isBlank(importRuleName)){
						importRuleName = "ScanResultList"+DateUtil.dateToStrYYYMMDD(new Date());
					}
					
					String fileName = f.getAbsolutePath()+Constant.SYSTEM_SEPARATOR+importRuleName+GatherConstant.RULE_RESULT_NAME_EXTENSION;
					final ExportResult executeRule = new ExportResult(tempCrawlerRuleBean.getRuleId(),fileName);
					SwingUtilities.invokeLater(new Runnable() {
						public void run() {
							//开启线程
							Thread currThread = new Thread(executeRule);
							currThread.start();
							Thread waitingThread = new Thread(new WaitingDialog(crawlerMainFrame,currThread,LanguageLoader.getString("Scan.exportInit")));
							waitingThread.start();
						}
					});
					
				}
			}
		}
	}
	class ExportResult implements Runnable{
        private Integer ruleId;
        private String fileName;
        
        public ExportResult(Integer ruleId,String fileName){
        	this.ruleId = ruleId;
        	this.fileName = fileName;
        }
		/* (non-Javadoc)
		 * @see java.lang.Runnable#run()
		 */
		@Override
		public void run() {
			CrawlerHistoryCriteria q = new CrawlerHistoryCriteria();
			q.setRuleId(ruleId);
			List<CrawlerHistoryBean> historyResult = crawlerHistoryService.getList(q, GatherConstant.SQLMAP_ID_GET_LIST_CRAWLER_CONTENT_HISTORY);
			String ruleStr = JsonUtils.formatObjectToJsonString(historyResult);
			try {
				FileUtils.writeByteArrayToFile(new File(fileName), ruleStr.getBytes());
			} catch (IOException e1) {
				e1.printStackTrace();
				logger.error("导出结果失败："+e1.getMessage());
			}
		}
	}
}
