package org.javacoo.cowswing.plugin.gather.service.export.handler;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import net.sf.jxls.exception.ParsePropertyException;
import net.sf.jxls.transformer.XLSTransformer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;
import org.javacoo.cowswing.plugin.gather.service.export.bean.ExportContentBean;
import org.javacoo.cowswing.plugin.gather.service.export.bean.ExportParamBean;
import org.javacoo.cowswing.plugin.gather.service.export.manager.ExcelTemplateManager;



/**
 *@author DuanYong
 *@since 2012-7-26下午5:06:48
 *@version 1.0
 */
public class ExcelTemplateHandler implements TemplateHandler {
	protected Log logger = LogFactory.getLog(ExcelTemplateHandler.class);
	/**excel工作表*/
    protected Workbook workbook;
    private ExcelTemplateManager excelTemplateManager;
    private String strTemplateFile;
    /**
	 * 根据配置文件初始化 templateConfigs
	 */
	public void init() {
		excelTemplateManager = new ExcelTemplateManager(strTemplateFile);
	}
    /**
	 * 取得模板
	 * @param templateName
	 * @return
	 */
	protected InputStream getTemplate(String templateName) {
		try {
			return this.excelTemplateManager.findTemplate(templateName).getInputStream();
		}catch (IOException e) {
			e.printStackTrace();
			logger.error("读取模板失败");
		}
		return null;
    }
    /**
	 * 将excel转换为html
	 */
	private void excelToHtml(ExportContentBean exportContentBean){
		try {
			InputStream in = new ByteArrayInputStream(exportContentBean.getByteContent());
			HSSFWorkbook wb = new HSSFWorkbook(in);
			ExcelToHtmlHelper eth = new ExcelToHtmlHelper();
			exportContentBean.setStringContent(eth.getExcelInfo(wb));
			in.close();
		}
		catch (IOException e) {
			e.printStackTrace();
			logger.error("将excel转换为html失败");
		}
		catch (Exception e) {
			e.printStackTrace();
			logger.error("将excel转换为html失败");
		}
	} 
	
	/**
     * 组装模板
     */
	public void populateTemplate(ExportContentBean exportContentBean,ExportParamBean exportParamBean){
		InputStream in = getTemplate(exportParamBean.getTemplateName());
		XLSTransformer transformer = new XLSTransformer();
		try {
			this.workbook = transformer.transformXLS(in, exportParamBean.getResultMap());
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			this.workbook.write(byteArrayOutputStream);
			exportContentBean.setByteContent(byteArrayOutputStream.toByteArray());
			excelToHtml(exportContentBean);
		}
		catch (ParsePropertyException e) {
			e.printStackTrace();
			logger.error("解析模板属性失败");
		}
		catch (InvalidFormatException e) {
			e.printStackTrace();
			logger.error("模板格式校验失败");
		}
		catch (IOException e) {
			e.printStackTrace();
			logger.error("组装模板失败");
		}
	}
	public String getStrTemplateFile() {
		return strTemplateFile;
	}
	public void setStrTemplateFile(String strTemplateFile) {
		this.strTemplateFile = strTemplateFile;
	}
	
	
    
}
