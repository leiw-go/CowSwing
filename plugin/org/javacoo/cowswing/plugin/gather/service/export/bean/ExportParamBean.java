package org.javacoo.cowswing.plugin.gather.service.export.bean;

import java.util.Map;

/**
 * 导出数据参数值对象
 * <p>
 * 说明:
 * </p>
 * <li></li>
 * @author DuanYong
 * @since 2016-2-18上午10:57:55
 * @version 1.0
 */
public class ExportParamBean {
	/**模板名称*/
	private String templateName;
	/**生成的文件名*/
	private String fileName;
	/**生成的文件类型*/
	private String fileType;
	/**数据Map*/
	private Map<Object,Object> resultMap;
	public String getTemplateName() {
		return templateName;
	}
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFileType() {
		return fileType;
	}
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	public Map<Object, Object> getResultMap() {
		return resultMap;
	}
	public void setResultMap(Map<Object, Object> resultMap) {
		this.resultMap = resultMap;
	}
	
	
}
