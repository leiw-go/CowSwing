package org.javacoo.cowswing.plugin.gather.service.export.builder;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.apache.poi.poifs.filesystem.DirectoryEntry;
import org.apache.poi.poifs.filesystem.DocumentEntry;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.javacoo.cowswing.plugin.gather.constant.GatherConstant;
import org.springframework.stereotype.Component;


/**
 * 生成WORD
 * @author DuanYong
 * 2012-07-23
 */
@Component(GatherConstant.EXPORT_TYPE_WORD)
public class ExportWordBuilder extends AbstractExportBeanBuilder{
	private final static String WORD_FILE_EXTENSION = ".doc";
    private final static String WORD_CONTENT_TYPE = "application/msword";
	@Override
	protected void buildExportContentBean() {
		this.exportContentBean.setDownloadType(WORD_CONTENT_TYPE);
		this.exportContentBean.setFileName(this.exportParamBean.getFileName() + WORD_FILE_EXTENSION);
		this.exportContentBean.setByteContent(getByteContent());
	}
	
	/**
     * 取得组装好的PDF文件 byte[]
     * @return
     */
	private byte[] getByteContent(){
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		POIFSFileSystem poifs = new POIFSFileSystem();
		DirectoryEntry directory = poifs.getRoot();
		try {
			DocumentEntry documentEntry = directory.createDocument(
			"WordDocument", new ByteArrayInputStream(this.exportContentBean.getStringContent().getBytes("GBK")));
			poifs.writeFilesystem(out);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try {
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			} 
		}
		
		return out.toByteArray();
	}
	

}
