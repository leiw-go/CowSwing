package org.javacoo.cowswing.plugin.gather.service.export.handler;

import java.io.CharArrayWriter;
import java.io.IOException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.javacoo.cowswing.plugin.gather.service.export.bean.ExportContentBean;
import org.javacoo.cowswing.plugin.gather.service.export.bean.ExportParamBean;
import org.javacoo.cowswing.plugin.gather.service.export.manager.FreemarkerTemplateManager;

import freemarker.template.Template;
import freemarker.template.TemplateException;

/**
 * Freemarker模板处理
 * <p>
 * 说明:
 * </p>
 * <li></li>
 * @author DuanYong
 * @since 2016-2-18上午10:50:23
 * @version 1.0
 */
public class FreemarkerTemplateHandler implements TemplateHandler{
	protected Log logger = LogFactory.getLog(FreemarkerTemplateHandler.class);
	private FreemarkerTemplateManager templateManager;
	private String strSingleTemplateFile;
	private String strMultipleTemplateFile;
	
	/**
	 * 根据配置文件初始化 templateConfigs
	 */
	public void init() {
		logger.info("TemplatePath..." + this.strSingleTemplateFile+","+strMultipleTemplateFile);
		templateManager = new FreemarkerTemplateManager(strSingleTemplateFile,strMultipleTemplateFile);
	}
	
	/**
     * 组装模板
     */
	public void populateTemplate(ExportContentBean exportContentBean,ExportParamBean exportParamBean){
		Template t = null;
		CharArrayWriter out = new CharArrayWriter();
		try {
			t = templateManager.findTemplate(exportParamBean.getTemplateName());
			t.process(exportParamBean.getResultMap(), out);
			if(StringUtils.isBlank(out.toString())){
				return;
			}
			//String str = new String(out.toString().getBytes(),"UTF-8");
			//exportContentBean.setStringContent(new String(out.toString().getBytes(),"UTF-8"));
			exportContentBean.setStringContent(out.toString());
			exportContentBean.setByteContent(out.toString().getBytes());
			logger.info("---Html文本内容" + out.toString());
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("生成html文本发生IO错误");
		} catch (TemplateException e) {
			e.printStackTrace();
			logger.error("解析模板发生错误");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("生成html文本发生未知错误");
		}
	}

	public String getStrSingleTemplateFile() {
		return strSingleTemplateFile;
	}

	public void setStrSingleTemplateFile(String strSingleTemplateFile) {
		this.strSingleTemplateFile = strSingleTemplateFile;
	}

	public String getStrMultipleTemplateFile() {
		return strMultipleTemplateFile;
	}

	public void setStrMultipleTemplateFile(String strMultipleTemplateFile) {
		this.strMultipleTemplateFile = strMultipleTemplateFile;
	}

	
    
}
