package org.javacoo.cowswing.plugin.gather.service.export.factory;

import org.javacoo.cowswing.plugin.gather.service.export.bean.ExportBean;

/**
 * 文件下载接口
 * <p>
 * 负责生成文件下载bean
 * 
 * <p>
 * 说明:
 * </p>
 * <li></li>
 * @author DuanYong
 * @since 2016-2-18下午3:42:02
 * @version 1.0
 */
public interface ExportFactory {
	/**
     * 生成下载文件
     */
	ExportBean createDownloadFile();
}
