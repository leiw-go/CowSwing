package org.javacoo.cowswing.plugin.gather.service.export.builder;


import org.javacoo.cowswing.plugin.gather.constant.GatherConstant;
import org.springframework.stereotype.Component;

/**
 * 生成Txt
 * <p>
 * 说明:
 * </p>
 * <li></li>
 * @author DuanYong
 * @since 2016-2-18下午3:26:52
 * @version 1.0
 */
@Component(GatherConstant.EXPORT_TYPE_TXT)
public class ExportTxtBuilder extends AbstractExportBeanBuilder{
	private final static String TXT_FILE_EXTENSION = ".txt";
    private final static String TXT_CONTENT_TYPE = "text/plain";
	@Override
	protected void buildExportContentBean() {
		this.exportContentBean.setDownloadType(TXT_CONTENT_TYPE);
		this.exportContentBean.setFileName(this.exportParamBean.getFileName() + TXT_FILE_EXTENSION);
		this.exportContentBean.setByteContent(getByteContent());
	}
    /**
     * 取得组装好的excel文件 byte[]
     * @return
     */
	private byte[] getByteContent(){
		return this.exportContentBean.getByteContent();
	}
    
	

}
