package org.javacoo.cowswing.plugin.gather.service.export.builder;

import org.javacoo.cowswing.plugin.gather.service.export.bean.ExportContentBean;
import org.javacoo.cowswing.plugin.gather.service.export.bean.ExportParamBean;

/**
 * 导出数据对象BEAN构建接口
 * <p>
 * 说明:
 * </p>
 * <li></li>
 * @author DuanYong
 * @since 2016-2-18下午2:27:26
 * @version 1.0
 */
public interface ExportBeanBuilder {
	/**
	 * 构建文件下载bean
	 * <p>
	 * 说明:
	 * </p>
	 * <li></li>
	 * @author DuanYong
	 * @since 2016-2-18下午2:28:16
	 * @version 1.0
	 * @param exportParamBean 参数bean
	 * @return 导出数据对象
	 */
	ExportContentBean build(ExportParamBean exportParamBean);
}
