/**
 * 如石子一粒,仰高山之巍峨,但不自惭形秽.
 * 若小草一棵,慕白杨之伟岸,却不妄自菲薄.
 */
package org.javacoo.cowswing.plugin.gather.service.cache;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.javacoo.cowswing.base.service.ICrawlerService;
import org.javacoo.cowswing.core.cache.support.AbstractCowSwingCache;
import org.javacoo.cowswing.core.cache.support.CacheKeyConstant;
import org.javacoo.cowswing.core.constant.Config;
import org.javacoo.cowswing.core.constant.Constant;
import org.javacoo.cowswing.core.event.CowSwingEvent;
import org.javacoo.cowswing.core.event.CowSwingEventType;
import org.javacoo.cowswing.core.event.CowSwingObserver;
import org.javacoo.cowswing.plugin.core.service.beans.CrawlerConfigBean;
import org.javacoo.cowswing.plugin.core.service.beans.CrawlerConfigCriteria;
import org.javacoo.cowswing.plugin.gather.constant.SystemConstant;
import org.springframework.stereotype.Component;

/**
 * 数据类型映射缓存
 * 
 * <p>
 * 说明:
 * </p>
 * <li></li>
 * @author DuanYong
 * @since 2016-1-25上午9:48:57
 * @version 1.0
 */
@Component("dataTypeMapCache")
public class DataTypeMapCache extends AbstractCowSwingCache<Map<String,Map<String,String>>>{
	
	/* (non-Javadoc)
	 * @see org.javacoo.crawler.cache.support.AbstractCrawlerCache#loadDataToCache()
	 */
	@Override
	protected Map<String,Map<String,String>> loadDataToCache() {
		Map<String,Map<String,String>> returnMap = new HashMap<String,Map<String,String>>();
		Map<String,String> dataTypeMap = Config.CRAWLER_CONFIG_MAP.get(Config.CRAWLER_CONFIG_KEY_DATA_TYPE_MAP);
		
		if(null != dataTypeMap && !dataTypeMap.isEmpty()){
			Map<String,String> itemMap = null;
			for(String key : dataTypeMap.keySet()){
				itemMap = new HashMap<String,String>();
				parserString(dataTypeMap.get(key),itemMap);
				if(!itemMap.isEmpty()){
					returnMap.put(key, itemMap);
				}
			}
		}
		return returnMap;
	}

    /**
     * 解析数据类型字符串
     * <p>
     * 说明:
     * </p>
     * <li></li>
     * @author DuanYong
     * @since 2016-1-25上午9:59:46
     * @version 1.0
     * @param string
     * @param itemMap
     */
	private void parserString(String string, Map<String, String> itemMap) {
		if(StringUtils.isBlank(string)){
			return;
		}
		String[] typeArr = string.split(Config.CRAWLER_CONFIG_KEY_DATA_TYPE_SEP1);
		String[] tempArr = null;
		for(String type : typeArr){
			if(!type.contains(Config.CRAWLER_CONFIG_KEY_DATA_TYPE_SEP2)){
				continue;
			}
			tempArr = type.split(Config.CRAWLER_CONFIG_KEY_DATA_TYPE_SEP2);
			itemMap.put(tempArr[0], tempArr[1]);
		}
	}


	/* (non-Javadoc)
	 * @see org.javacoo.crawler.cache.ICrawlerCache#getCacheKey()
	 */
	@Override
	public String getCacheKey() {
		return CacheKeyConstant.CACHE_KEY_DATA_TYPE_MAP;
	}

	
}
