package org.javacoo.cowswing.plugin.gather.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.javacoo.cowswing.base.service.ICrawlerService;
import org.javacoo.cowswing.core.cache.ICowSwingCacheManager;
import org.javacoo.cowswing.core.constant.Constant;
import org.javacoo.cowswing.core.context.CowSwingContextData;
import org.javacoo.cowswing.core.event.CowSwingEvent;
import org.javacoo.cowswing.core.event.CowSwingEventType;
import org.javacoo.cowswing.core.event.CowSwingListener;
import org.javacoo.cowswing.core.event.CowSwingObserver;
import org.javacoo.cowswing.core.loader.LanguageLoader;
import org.javacoo.cowswing.core.utils.DateFormatUtils;
import org.javacoo.cowswing.core.utils.DateUtil;
import org.javacoo.cowswing.plugin.gather.constant.GatherConstant;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerContentBean;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerContentCommentBean;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerContentCommentCriteria;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerContentCriteria;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerContentPaginationBean;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerContentPaginationCriteria;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerContentResourceBean;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerContentResourceCriteria;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerExtendFieldCriteria;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerHistoryBean;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerHistoryCriteria;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerRuleBean;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerRuleCriteria;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerTaskBean;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerTaskCriteria;
import org.javacoo.cowswing.plugin.gather.service.export.ExportHelper;
import org.javacoo.crawler.core.constants.Constants;
import org.javacoo.crawler.core.data.CrawlerExtendFieldBean;
import org.javacoo.crawler.core.data.GoogleBloomFilter;
import org.javacoo.crawler.core.data.Task;
import org.javacoo.crawler.core.data.uri.CrawlResURI;
import org.javacoo.crawler.core.persistent.CrawlerPersistent;
import org.javacoo.crawler.core.util.File.DefaultFileHelper;
import org.javacoo.crawler.core.util.File.FileHelper;
import org.javacoo.crawler.core.util.parser.impl.RegexParserImpl;
import org.javacoo.persistence.PaginationSupport;
import org.javacoo.persistence.util.DBConnectionManager;
import org.springframework.stereotype.Service;
import org.wltea.analyzer.core.IKSegmenter;
import org.wltea.analyzer.core.Lexeme;
import org.wltea.analyzer.lucene.IKAnalyzer;

import com.javacoo.webservice.manager.ManagerService;
import com.javacoo.webservice.manager.beans.UserBean;



/**
 * 爬虫持久层接口实现类
 * @author javacoo
 * @since 2011-11-12
 */
@Service("crawlerPersistent")
public class SimpleCrawlerPersistentImpl implements CrawlerPersistent,CowSwingListener{
	protected Logger log = Logger.getLogger(this.getClass());
	/**URL缓存*/
	private static Map<Integer,GoogleBloomFilter> cache = new ConcurrentHashMap<Integer,GoogleBloomFilter>();
	/**规则缓存*/
	private static Map<Integer,CrawlerRuleBean> crawlerRuleCache = new ConcurrentHashMap<Integer,CrawlerRuleBean>();
	/**任务缓存*/
	private static Map<Integer,CrawlerTaskBean> crawlerTaskCache = new ConcurrentHashMap<Integer,CrawlerTaskBean>();
	
	/**任务服务类*/
	@Resource(name="crawlerTaskService")
	private ICrawlerService<CrawlerTaskBean,CrawlerTaskCriteria> crawlerTaskService;
	/**规则服务类*/
	@Resource(name="crawlerRuleService")
	private ICrawlerService<CrawlerRuleBean,CrawlerRuleCriteria> crawlerRuleService;
	/**采集内容服务类*/
	@Resource(name="crawlerContentService")
	private ICrawlerService<CrawlerContentBean,CrawlerContentCriteria> crawlerContentService;
	/**采集内容评论服务类*/
	@Resource(name="crawlerContentCommentService")
	private ICrawlerService<CrawlerContentCommentBean,CrawlerContentCommentCriteria> crawlerContentCommentService;
	/**采集内容分页服务类*/
	@Resource(name="crawlerContentPaginationService")
	private ICrawlerService<CrawlerContentPaginationBean,CrawlerContentPaginationCriteria> crawlerContentPaginationService;
	/**采集内容资源服务类*/
	@Resource(name="crawlerContentResourceService")
	private ICrawlerService<CrawlerContentResourceBean,CrawlerContentResourceCriteria> crawlerContentResourceService;
	/**扩展字段服务类*/
	@Resource(name="crawlerExtendFieldService")
	private ICrawlerService<CrawlerExtendFieldBean,CrawlerExtendFieldCriteria> crawlerExtendFieldService;
	/**采集历史*/
	@Resource(name="crawlerHistoryService")
	private ICrawlerService<CrawlerHistoryBean,CrawlerHistoryCriteria> crawlerHistoryService;
	/**数据导出帮助类*/
	@Resource(name="exportHelper")
	private ExportHelper exportHelper;
	/**文件帮助类*/
	private FileHelper fileHelper = new DefaultFileHelper();
	/**数据库连接管理*/
	private DBConnectionManager connectionManager;
	/**缓存管理*/
	@Resource(name="cowSwingCacheManager")
	private ICowSwingCacheManager crawlerCacheManager;
	/**保存内容至远程数据库服务*/
	private SaveRemoteContent saveRemoteContent;
	/**正则表达式解析器*/
	private org.javacoo.crawler.core.util.parser.Parser regexParser;
	/**分词器*/
	private Analyzer analyzer;
	
	public SimpleCrawlerPersistentImpl(){
		regexParser = new RegexParserImpl();
		analyzer = new IKAnalyzer(true);
		CowSwingObserver.getInstance().addCrawlerListener(this);
	}
	/**
	 * 保存内容
	 * @param task
	 */
	public synchronized void save(Task task) {
		CrawlerRuleBean crawlerRuleBean = getCrawlerRuleBeanFromCache(task.getController().getCrawlScope().getId());
		//更新任务表
		String desc = Constants.EMPTY_STR;
		if(task.getController().getCrawlScope().isOpenMonitor()){
			desc = DateUtil.dateToStr( new Date(), "yyyy-MM-dd HH:mm:ss")+Constants.EMPTY_SPLIT_STR+LanguageLoader.getString("CrawlerMainFrame.CrawlGatherComplate") +Constants.EMPTY_SPLIT_STR+ task.getContentBean().getTitle();
		}
		//如果设置了采集关键字，则只保存包含此关键字的内容
		if(!isExistKewords(crawlerRuleBean,task,desc)){
			return;
		}
		
		//如果未设置内容采集参数,且设置了扩展字段
		if(task.getController().getCrawlScope().isOnlyGatherList() && !task.getContentBean().getFieldValueMapList().isEmpty()){
			Map<String,List<CrawlerExtendFieldBean>> resultList = getExtendFieldValuesForList(task);
			for(String key : resultList.keySet()){
				task.getContentBean().setExtendFieldList(resultList.get(key));
				saveCrawlerRuleBean(task,crawlerRuleBean);
			}
		}else{
			List<CrawlerExtendFieldBean> crawlerExtendFieldList = getExtendFieldList(task);
			task.getContentBean().setExtendFieldList(crawlerExtendFieldList);
			saveCrawlerRuleBean(task,crawlerRuleBean);
		}
		
		if(Boolean.valueOf(crawlerRuleBean.getRuleDataBaseBean().getSaveToDataBaseFlag()) && Boolean.valueOf(crawlerRuleBean.getRuleDataBaseBean().getOnlySaveRemoteataBaseFlag())){
			updateTask(task.getController().getCrawlScope().getId(),task.getController().getFrontier().getTaskSize(),desc,GatherConstant.TASK_TYPE_3,CowSwingEventType.SaveStatusChangeEvent);
		}else{
			updateTask(task.getController().getCrawlScope().getId(),task.getController().getFrontier().getTaskSize(),desc,GatherConstant.TASK_TYPE_1,CowSwingEventType.TaskStatusChangeEvent);
		}
	}
	private void saveCrawlerRuleBean(Task task,CrawlerRuleBean crawlerRuleBean){
		
		
		if(Boolean.valueOf(crawlerRuleBean.getRuleDataBaseBean().getSaveToDataBaseFlag()) && Boolean.valueOf(crawlerRuleBean.getRuleDataBaseBean().getOnlySaveRemoteataBaseFlag())){
			if(null == saveRemoteContent){
				saveRemoteContent = new SaveRemoteContent(crawlerRuleService,
						crawlerContentService,
						crawlerContentCommentService,
						crawlerContentPaginationService,
						crawlerExtendFieldService,
						this.getCrawlerTaskService(),
						crawlerContentResourceService,
						this.getConnectionManager(),
						this.crawlerCacheManager,
						crawlerRuleBean.getRuleId());
			}
			task.getContentBean().setUrl(task.getCrawlURI().getUrl().getUrl());
			saveRemoteContent.insertContent(task.getContentBean(), crawlerRuleBean);
		}else{
			//插入内容
			insertContent(task,crawlerRuleBean);
		}
	}
	private Map<String,List<CrawlerExtendFieldBean>> getExtendFieldValuesForList(Task task) {
		CrawlerExtendFieldBean crawlerExtendFieldBean = null;
		List<CrawlerExtendFieldBean> crawlerExtendFieldList = null;
		
		Map<String,List<CrawlerExtendFieldBean>> resultListMap = new HashMap<String,List<CrawlerExtendFieldBean>>();
		int i = 0;
		for(Map<String,String> fieldValueMap : task.getContentBean().getFieldValueMapList()){
			if(fieldValueMap == null || fieldValueMap.isEmpty()){
				continue;
			}
			crawlerExtendFieldList = new ArrayList<CrawlerExtendFieldBean>();
			for(Map.Entry<String,String> entry : fieldValueMap.entrySet()){
				if(null != entry && StringUtils.isNotBlank(entry.getKey())&& StringUtils.isNotBlank(entry.getValue())){
					crawlerExtendFieldBean = new CrawlerExtendFieldBean();
					crawlerExtendFieldBean.setRuleId(task.getController().getCrawlScope().getId());
					crawlerExtendFieldBean.setFieldName(entry.getKey());
					crawlerExtendFieldBean.setFieldValue(entry.getValue());
					crawlerExtendFieldList.add(crawlerExtendFieldBean);
				}
			}
			resultListMap.put(String.valueOf(i), crawlerExtendFieldList);
			i++;
		}
		return resultListMap;
	}
	private Map<String,List<CrawlerExtendFieldBean>> getExtendFieldValues(Task task) {
		String key = "";
		String value = "";
		String[] valueArray = null;
		CrawlerExtendFieldBean crawlerExtendFieldBean = null;
		List<CrawlerExtendFieldBean> crawlerExtendFieldList = null;
		

		//取得数据大小
		int len = 0;
		Map<String,String> tempMap = null;
		List<Map<String,String>> valueList = null;
		Map<String,List<Map<String,String>>> tempListMap = new HashMap<String,List<Map<String,String>>>();
		for(Iterator<String> it = task.getContentBean().getFieldValueMap().keySet().iterator();it.hasNext();){
			key = it.next();
			value = task.getContentBean().getFieldValueMap().get(key);
			valueArray = value.split(Constants.EXTEND_SPLIT_KEY);
			if(len < valueArray.length){
				len = valueArray.length;
			}
			valueList = new ArrayList<Map<String,String>>();
			for(String v : valueArray){
				tempMap = new HashMap<String,String>();
				tempMap.put(key, v);
				valueList.add(tempMap);
			}
			tempListMap.put(key, valueList);
		}
		Map<String,List<CrawlerExtendFieldBean>> resultListMap = new HashMap<String,List<CrawlerExtendFieldBean>>();
		for(int i = 0;i < len;i++){
			crawlerExtendFieldList = new ArrayList<CrawlerExtendFieldBean>();
			for(String k : tempListMap.keySet()){
				valueList = tempListMap.get(k);
				if(i >= valueList.size()){
					break;
				}
				tempMap = valueList.get(i);
				if(null != tempMap && !tempMap.isEmpty() && tempMap.containsKey(k)){
					value = tempMap.get(k);
					crawlerExtendFieldBean = new CrawlerExtendFieldBean();
					crawlerExtendFieldBean.setRuleId(task.getController().getCrawlScope().getId());
					crawlerExtendFieldBean.setFieldName(k);
					crawlerExtendFieldBean.setFieldValue(value);
					crawlerExtendFieldList.add(crawlerExtendFieldBean);
				}
			}
			if(!crawlerExtendFieldList.isEmpty()){
				resultListMap.put(String.valueOf(i), crawlerExtendFieldList);
			}
		}
		return resultListMap;
	}
	private List<CrawlerExtendFieldBean> getExtendFieldList(Task task){
		CrawlerExtendFieldBean crawlerExtendFieldBean = null;
		List<CrawlerExtendFieldBean> crawlerExtendFieldList = new ArrayList<CrawlerExtendFieldBean>();
		String key = "";
		for(Iterator<String> it = task.getContentBean().getFieldValueMap().keySet().iterator();it.hasNext();){
			key = it.next();
			crawlerExtendFieldBean = new CrawlerExtendFieldBean();
			crawlerExtendFieldBean.setRuleId(task.getController().getCrawlScope().getId());
			crawlerExtendFieldBean.setFieldName(key);
			crawlerExtendFieldBean.setFieldValue(task.getContentBean().getFieldValueMap().get(key));
			crawlerExtendFieldList.add(crawlerExtendFieldBean);
		}
		return crawlerExtendFieldList;
	}
	/**
	 * 检查内容是否包 含关键字
	 * @param crawlerRuleBean 规则对象
	 * @param task 任务
	 * @param desc 描述
	 * @return 存在返回true
	 */
	private boolean isExistKewords(CrawlerRuleBean crawlerRuleBean,Task task,String desc){
		//getKeywords(task.getContentBean().getContentPlainText());
		//如果设置了采集关键字，则只保存包含此关键字的内容
		if(null != task.getController().getCrawlScope().getKeywords() && StringUtils.isNotBlank(task.getContentBean().getContentPlainText())){
			List<String> keywords = new ArrayList<String>();
			List<String> tempKeywordsList = null;
			for(String key : task.getController().getCrawlScope().getKeywords()){
				tempKeywordsList = regexParser.getList(task.getContentBean().getContentPlainText(), key);
				if(CollectionUtils.isNotEmpty(tempKeywordsList)){
					keywords.addAll(tempKeywordsList);
				}
			}
			//如果未包含则不保存
			if(CollectionUtils.isNotEmpty(keywords)){
				if(Boolean.valueOf(crawlerRuleBean.getRuleDataBaseBean().getSaveToDataBaseFlag()) && Boolean.valueOf(crawlerRuleBean.getRuleDataBaseBean().getOnlySaveRemoteataBaseFlag())){
					updateTask(task.getController().getCrawlScope().getId(),task.getController().getFrontier().getTaskSize(),desc,GatherConstant.TASK_TYPE_3,CowSwingEventType.SaveStatusChangeEvent);
				}else{
					updateTask(task.getController().getCrawlScope().getId(),task.getController().getFrontier().getTaskSize(),desc,GatherConstant.TASK_TYPE_1,CowSwingEventType.TaskStatusChangeEvent);
				}
				return false;
			}
			task.getContentBean().setKeywords(keywords.toString());
		}
		return true;
	}
	/**
	 * 更新任务表
	 * <p>方法说明:</>
	 * <li></li>
	 * @author DuanYong
	 * @since 2013-4-30 下午3:56:37
	 * @version 1.0
	 * @exception 
	 * @param ruleId 规则ID
	 * @param total 总数
	 * @param type 任务类型
	 * @param CowSwingEventType 事件类型
	 */
	private void updateTask(int ruleId,int total,String desc,String type,CowSwingEventType CowSwingEventType){
		CrawlerTaskBean crawlerTaskBean = getCrawlerTaskBeanFromCache(ruleId,type);
		crawlerTaskBean.setTotal(total);
		crawlerTaskBean.setDesc(desc);
		crawlerTaskBean.setComplete(crawlerTaskBean.getComplete() + 1);
		crawlerTaskBean.setCowSwingEvent(new CowSwingEvent(this,CowSwingEventType,crawlerTaskBean));
		getCrawlerTaskService().update(crawlerTaskBean, GatherConstant.SQLMAP_ID_UPDATE_COMPLETE_CRAWLER_TASK);
	}
	
	
    /**
     * 插入内容及相关内容
     * <p>方法说明:</p>
     * @auther DuanYong
     * @since 2012-11-28 上午10:24:21
     * @param task
     * @return void
     */
	private void insertContent(Task task,CrawlerRuleBean rule) {

		//组装返回信息
		StringBuilder info = null;
		
        // 组装内容
		CrawlerContentBean crawlerContentBean = new CrawlerContentBean();
		crawlerContentBean.setContent(task.getContentBean().getContent());
		crawlerContentBean.setContentPlainText(task.getContentBean().getContentPlainText());
		crawlerContentBean.setRuleId(task.getController().getCrawlScope().getId());
		crawlerContentBean.setSaveDate(new Date());
		crawlerContentBean.setViewDate(getRandomDate(rule));
		if(StringUtils.isBlank(task.getContentBean().getTitle())){
			if(!CollectionUtils.isEmpty(task.getContentBean().getExtendFieldList())){
				crawlerContentBean.setTitle(task.getContentBean().getExtendFieldList().toString());
			}else{
				crawlerContentBean.setTitle(rule.getRuleBaseBean().getRuleName());
			}
		}else{
			crawlerContentBean.setTitle(task.getContentBean().getTitle());
		}
		crawlerContentBean.setBrief(task.getContentBean().getBrief());
		crawlerContentBean.setTitleImg(task.getContentBean().getTitleImg());
		crawlerContentBean.setUrl(task.getCrawlURI().getUrl().getUrl());
		if(task.getController().getCrawlScope().isOpenMonitor()){
			info = new StringBuilder();
			info.append(task.getController().getCrawlScope().getId());
			info.append(":");
			info.append(DateUtil.dateToStr(new Date(), "yyyy-MM-dd HH:mm:ss"));
			info.append(Constants.EMPTY_SPLIT_STR);
			info.append(LanguageLoader.getString("CrawlerMainFrame.CrawlGatherComplateContent"));
			info.append(crawlerContentBean.getTitle());
			crawlerContentBean.setCowSwingEvent(new CowSwingEvent(this,CowSwingEventType.ContentTableAddEvent,info.toString()));
		}
		//插入内容
		int contentId = getCrawlerContentService().insert(crawlerContentBean,GatherConstant.SQLMAP_ID_INSERT_CRAWLER_CONTENT);
		crawlerContentBean.setContentId(contentId);
		
		//插入评论
		if(CollectionUtils.isNotEmpty(task.getContentBean().getCommentList())){
			CrawlerContentCommentBean crawlerContentCommentBean = null;
			for(String comment : task.getContentBean().getCommentList()){
				crawlerContentCommentBean = new CrawlerContentCommentBean();
				crawlerContentCommentBean.setContent(comment);
				crawlerContentCommentBean.setRuleId(task.getController().getCrawlScope().getId());
				crawlerContentCommentBean.setContentId(crawlerContentBean.getContentId());
				if(task.getController().getCrawlScope().isOpenMonitor()){
					info = new StringBuilder();
					info.append(task.getController().getCrawlScope().getId());
					info.append(":");
					info.append(DateUtil.dateToStr(new Date(), "yyyy-MM-dd HH:mm:ss"));
					info.append(Constants.EMPTY_SPLIT_STR);
					info.append(LanguageLoader.getString("CrawlerMainFrame.CrawlGatherComplateComment"));
					info.append(comment);
					crawlerContentCommentBean.setCowSwingEvent(new CowSwingEvent(this,CowSwingEventType.ContentCommentTableAddEvent,info.toString()));
				}
				getCrawlerContentCommentService().insert(crawlerContentCommentBean, GatherConstant.SQLMAP_ID_INSERT_CRAWLER_CONTENT_COMMENT);
			}
		}
		//插入分页内容
		if(CollectionUtils.isNotEmpty(task.getContentBean().getContentList())){
			CrawlerContentPaginationBean crawlerContentPaginationBean = null;
			for(String content : task.getContentBean().getContentList()){
				crawlerContentPaginationBean = new CrawlerContentPaginationBean();
				crawlerContentPaginationBean.setContent(content);
				crawlerContentPaginationBean.setRuleId(task.getController().getCrawlScope().getId());
				crawlerContentPaginationBean.setContentId(crawlerContentBean.getContentId());
				if(task.getController().getCrawlScope().isOpenMonitor()){
					info = new StringBuilder();
					info.append(task.getController().getCrawlScope().getId());
					info.append(":");
					info.append(DateUtil.dateToStr(new Date(), "yyyy-MM-dd HH:mm:ss"));
					info.append(Constants.EMPTY_SPLIT_STR);
					info.append(LanguageLoader.getString("CrawlerMainFrame.CrawlGatherComplateContentPage"));
					info.append(content);
					crawlerContentPaginationBean.setCowSwingEvent(new CowSwingEvent(this,CowSwingEventType.ContentPaginationTableAddEvent,info.toString()));
				}
				getCrawlerContentPaginationService().insert(crawlerContentPaginationBean, GatherConstant.SQLMAP_ID_INSERT_CRAWLER_CONTENT_PAGINATION);
			}
		}
		//插入资源
		if(CollectionUtils.isNotEmpty(task.getContentBean().getResCrawlURIList()) && task.getController().getCrawlScope().isExtractContentRes()){
			CrawlerContentResourceBean crawlerContentResourceBean = null;
			for(CrawlResURI crawlResURI : task.getContentBean().getResCrawlURIList()){
				if(StringUtils.isNotBlank(crawlResURI.getUrl().getUrl())){
					crawlerContentResourceBean = new CrawlerContentResourceBean();
					crawlerContentResourceBean.setName(FilenameUtils.getName(crawlResURI.getUrl().getUrl()));
					crawlerContentResourceBean.setPath(crawlResURI.getUrl().getUrl());
					crawlerContentResourceBean.setRuleId(task.getController().getCrawlScope().getId());
					crawlerContentResourceBean.setType(FilenameUtils.getExtension(crawlResURI.getUrl().getUrl()));
					crawlerContentResourceBean.setContentId(crawlerContentBean.getContentId());
					if(crawlResURI.getUrl().getUrl().toLowerCase().startsWith(Constant.HTTP) || crawlResURI.getUrl().getUrl().toLowerCase().startsWith(Constant.HTTPS)){
						crawlerContentResourceBean.setIsLocal(Constant.NO);	
					}else{
						crawlerContentResourceBean.setIsLocal(Constant.YES);
					}
					if(task.getController().getCrawlScope().isOpenMonitor()){
						info = new StringBuilder();
						info.append(task.getController().getCrawlScope().getId());
						info.append(":");
						info.append(DateUtil.dateToStr(new Date(), "yyyy-MM-dd HH:mm:ss"));
						info.append(Constants.EMPTY_SPLIT_STR);
						info.append(LanguageLoader.getString("CrawlerMainFrame.CrawlGatherComplateRes"));
						info.append(crawlResURI.getOriginResUrl());
						crawlerContentResourceBean.setCowSwingEvent(new CowSwingEvent(this,CowSwingEventType.ContentResourceTableAddEvent,info.toString()));
					}
					getCrawlerContentResourceService().insert(crawlerContentResourceBean, GatherConstant.SQLMAP_ID_INSERT_CRAWLER_CONTENT_RESOURCE);
				}
			}
		}
		//插入扩展字段
		if(!task.getContentBean().getExtendFieldList().isEmpty()){
			for(CrawlerExtendFieldBean crawlerExtendFieldBean : task.getContentBean().getExtendFieldList()){
				crawlerExtendFieldBean.setContentId(contentId);
				if(task.getController().getCrawlScope().isOpenMonitor()){
					info = new StringBuilder();
					info.append(task.getController().getCrawlScope().getId());
					info.append(":");
					info.append(DateUtil.dateToStr(new Date(), "yyyy-MM-dd HH:mm:ss"));
					info.append(Constants.EMPTY_SPLIT_STR);
					info.append(LanguageLoader.getString("CrawlerMainFrame.CrawlGatherComplateExtendField"));
					info.append(crawlerExtendFieldBean.getFieldName());
					crawlerExtendFieldBean.setCowSwingEvent(new CowSwingEvent(this,CowSwingEventType.ContentExtendFieldAddEvent,info.toString()));
				}
				crawlerExtendFieldService.insert(crawlerExtendFieldBean, GatherConstant.SQLMAP_ID_INSERT_CRAWLER_EXTEND_FIELD);
			}
		}
//		if(null != task.getContentBean().getFieldValueMap() && !task.getContentBean().getFieldValueMap().isEmpty()){
//			String key = "";
//			CrawlerExtendFieldBean crawlerExtendFieldBean = null;
//			for(Iterator<String> it = task.getContentBean().getFieldValueMap().keySet().iterator();it.hasNext();){
//				key = it.next();
//				crawlerExtendFieldBean = new CrawlerExtendFieldBean();
//				crawlerExtendFieldBean.setRuleId(task.getController().getCrawlScope().getId());
//				crawlerExtendFieldBean.setContentId(contentId);
//				crawlerExtendFieldBean.setFieldName(key);
//				crawlerExtendFieldBean.setFieldValue(task.getContentBean().getFieldValueMap().get(key));
//				if(task.getController().getCrawlScope().isOpenMonitor()){
//					info = new StringBuilder();
//					info.append(task.getController().getCrawlScope().getId());
//					info.append(":");
//					info.append(DateUtil.dateToStr(new Date(), "yyyy-MM-dd HH:mm:ss"));
//					info.append(Constants.EMPTY_SPLIT_STR);
//					info.append(LanguageLoader.getString("CrawlerMainFrame.CrawlGatherComplateExtendField"));
//					info.append(key);
//					crawlerExtendFieldBean.setCowSwingEvent(new CowSwingEvent(this,CowSwingEventType.ContentExtendFieldAddEvent,info.toString()));
//				}
//				crawlerExtendFieldService.insert(crawlerExtendFieldBean, GatherConstant.SQLMAP_ID_INSERT_CRAWLER_EXTEND_FIELD);
//			}
//		}
		//如果是扫描，或者历史记录不存在
		if(Constant.SCAN.equals(task.getController().getCrawlScope().getRuleType()) || !checkHistory(task)){
			//插入采集历史
			CrawlerHistoryBean crawlerHistoryBean = new CrawlerHistoryBean();
			crawlerHistoryBean.setContentId(contentId);
			crawlerHistoryBean.setRuleId(task.getController().getCrawlScope().getId());
			crawlerHistoryBean.setDate(new Date());
			if(StringUtils.isBlank(task.getContentBean().getTitle())){
				crawlerHistoryBean.setTitle(rule.getRuleBaseBean().getRuleName());
			}else{
				crawlerHistoryBean.setTitle(task.getContentBean().getTitle());
			}
			crawlerHistoryBean.setUrl(task.getCrawlURI().getUrl().getUrl());
			crawlerHistoryBean.setDescription(task.getContentBean().getBrief());
			//组装返回信息
//			if(task.getController().getCrawlScope().isOpenMonitor()){
//				info = new StringBuilder();
//				info.append(LanguageLoader.getString("CrawlerMainFrame.CrawlGatherComplate"));
//				info.append(crawlerHistoryBean.getTitle());
//				info.append(",");
//				info.append(LanguageLoader.getString("RuleList.end_time"));
//				info.append(":");
//				info.append(DateUtil.dateToStr(crawlerHistoryBean.getDate(), "yyyy-MM-dd HH:mm:ss"));
//				crawlerHistoryBean.setCowSwingEvent(new CowSwingEvent(this,CowSwingEventType.RuleHistoryTableAddEvent,info.toString()));
//			}
			crawlerHistoryService.insert(crawlerHistoryBean, GatherConstant.SQLMAP_ID_INSERT_CRAWLER_CONTENT_HISTORY);
		}
	}
	/**
	 * 检查任务是否已经存在于历史表中
	 * <p>方法说明:</p>
	 * <li></li>
	 * @auther DuanYong
	 * @since 2013-3-15 下午10:38:17
	 * @param task
	 * @return
	 * @return boolean
	 */
    private boolean checkHistory(Task task){
    	boolean isExist = false;
    	CrawlerHistoryCriteria q = new CrawlerHistoryCriteria();
		q.setTitle(task.getContentBean().getTitle());
		q.setUrl(task.getCrawlURI().getUrl().getUrl());
		PaginationSupport<CrawlerHistoryBean> result = crawlerHistoryService.getPaginatedList(q, GatherConstant.SQLMAP_ID_GET_LIST_CRAWLER_CONTENT_HISTORY);
		if(CollectionUtils.isNotEmpty(result.getData())){
			isExist = true;
		}
		return isExist;
    }

	/**
	 * 完成采集任务
	 * @param id 采集任务ID
	 */
	public synchronized void finished(Integer id) {
		log.info("完成采集任务--"+id);
		CrawlerTaskBean crawlerTaskBean = new CrawlerTaskBean();
		crawlerTaskBean.setRuleId(id);
		if(Boolean.valueOf(getCrawlerRuleBeanFromCache(id).getRuleDataBaseBean().getSaveToDataBaseFlag()) && Boolean.valueOf(getCrawlerRuleBeanFromCache(id).getRuleDataBaseBean().getOnlySaveRemoteataBaseFlag())){
			crawlerTaskBean.setType(GatherConstant.TASK_TYPE_3);
		}else{
			crawlerTaskBean.setType(GatherConstant.TASK_TYPE_1);
		}
		crawlerTaskBean = getCrawlerTaskService().get(crawlerTaskBean, GatherConstant.SQLMAP_ID_GET_BY_RULEID_CRAWLER_TASK);
		//删除采集任务
		deleteTask(crawlerTaskBean);
		
		//删除缓存
		removeCache(id,crawlerTaskBean.getIsAuto());
		
		//采集任务完成,更新规则表
		CrawlerRuleBean crawlerRuleBean = new CrawlerRuleBean();
		crawlerRuleBean.setRuleId(id);
		crawlerRuleBean.setStatus(Constant.TASK_STATUS_STOP);
		crawlerRuleBean.setTotalItem(crawlerTaskBean.getComplete());
		crawlerRuleBean.setEndTime(new Date());
		//组装返回信息
		StringBuilder info = new StringBuilder();
		info.append(LanguageLoader.getString("CrawlerMainFrame.CrawlGatherComplateTask"));
		info.append(crawlerTaskBean.getRuleName());
		info.append(",");
		info.append(LanguageLoader.getString("RuleList.total"));
		info.append(":");
		info.append(crawlerTaskBean.getComplete());
		info.append(",");
		info.append(LanguageLoader.getString("RuleList.end_time"));
		info.append(":");
		info.append(DateUtil.dateToStr(crawlerRuleBean.getEndTime(), "yyyy-MM-dd HH:mm:ss"));
		crawlerRuleBean.setCowSwingEvent(new CowSwingEvent(this,CowSwingEventType.RuleTableChangeEvent,info.toString()));
		getCrawlerRuleService().update(crawlerRuleBean, GatherConstant.SQLMAP_ID_STOP_CRAWLER_RULE);
		//更新采集总数
		try{
			UserBean userBean = (UserBean)CowSwingContextData.getInstance().getContextDataByKey(Constant.CONTEXT_DATA_KEY_USERANME);
			if(null != userBean){
				userBean.setGatherSize(userBean.getGatherSize() + crawlerTaskBean.getComplete());
				ManagerService service = (ManagerService)CowSwingContextData.getInstance().getContextDataByKey(Constant.CONTEXT_DATA_KEY_WEBSERVICE);
				service.updateGatherSize(userBean.getId(), crawlerTaskBean.getComplete());
			}
		}catch(Exception e) { 
			log.error("更新用户状态失败："+e.getMessage());
			log.error(e);
		}
		//处理资源
		//dealWithResource(id);
		//上传资源
		//uploadResource(id);
		//保存内容至远程数据库
		//uploadContent(id);
	}
	/**
	 * 处理资源
	 * <p>方法说明:</>
	 * <li></li>
	 * @author DuanYong
	 * @since 2014-9-11 上午11:12:30
	 * @version 1.0
	 * @exception 
	 * @param id
	 */
	private void dealWithResource(Integer id) {
		//开启线程
		Thread currThread = new Thread(new DealWithResource(id,this.getCrawlerContentResourceService(),this.getCrawlerTaskService(),this.getCrawlerRuleService()));
		currThread.start();
	}
	/**
	 * 将指定规则的内容保存到远程数据库
	 * <p>方法说明:</>
	 * <li></li>
	 * @author DuanYong
	 * @since 2013-4-30 下午9:06:13
	 * @version 1.0
	 * @exception 
	 * @param ruleId
	 */
	private void uploadContent(Integer ruleId){
		//开启线程
		Thread currThread = new Thread(new SaveRemoteContent(crawlerRuleService,
				crawlerContentService,
				crawlerContentCommentService,
				crawlerContentPaginationService,
				crawlerExtendFieldService,
				this.getCrawlerTaskService(),
				crawlerContentResourceService,
				this.getConnectionManager(),
				this.crawlerCacheManager,
				ruleId));
			currThread.start();
	}
	
	/**
	 * 删除任务
	 * <p>方法说明:</>
	 * <li></li>
	 * @author DuanYong
	 * @since 2013-4-29 下午5:00:20
	 * @version 1.0
	 * @exception 
	 * @param ruleId
	 */
	private void deleteTask(CrawlerTaskBean crawlerTaskBean){
		if(Boolean.valueOf(getCrawlerRuleBeanFromCache(crawlerTaskBean.getRuleId()).getRuleDataBaseBean().getSaveToDataBaseFlag()) && Boolean.valueOf(getCrawlerRuleBeanFromCache(crawlerTaskBean.getRuleId()).getRuleDataBaseBean().getOnlySaveRemoteataBaseFlag())){
			crawlerTaskBean.setCowSwingEvent(new CowSwingEvent(this,CowSwingEventType.SaveFinishedEvent,crawlerTaskBean.getRuleId()));
		}else{
			crawlerTaskBean.setCowSwingEvent(new CowSwingEvent(this,CowSwingEventType.TaskFinishedEvent,crawlerTaskBean.getRuleId()));
		}
		getCrawlerTaskService().delete(crawlerTaskBean, GatherConstant.SQLMAP_ID_DELETE_BY_TASKID_CRAWLER_TASK);
	}
	/**
	 * 上传资源
	 * <p>方法说明:</>
	 * <li></li>
	 * @author DuanYong
	 * @since 2013-4-24 下午11:47:33
	 * @version 1.0
	 * @exception 
	 * @param ruleId
	 */
	private void uploadResource(Integer ruleId){
		//开启线程
		Thread currThread = new Thread(new FtpUploadResource(ruleId,this.getCrawlerContentResourceService(),this.getCrawlerTaskService(),this.getCrawlerRuleService()));
		currThread.start();
	}
	/**
	 * 数据处理
	 * <p>
	 * 说明:
	 * </p>
	 * <li></li>
	 * @author DuanYong
	 * @since 2016-2-18下午4:51:00
	 * @version 1.0
	 * @param ruleId
	 */
	private void dataHandle(Integer ruleId){
		//开启线程
		Thread currThread = new Thread(
				new DataHandle(
						ruleId,
						exportHelper,
						fileHelper,
						crawlerRuleService,
						crawlerContentService,
						crawlerContentCommentService,
						crawlerContentPaginationService,
						crawlerExtendFieldService,
						crawlerTaskService));
			currThread.start();
	}
	/**
	 * 检查采集任务
	 * 已经采集过 返回true
	 * 
	 * @param url 是否根据标题判断,否则以URL
	 * @param ruleId 规则ID
	 */
	public synchronized boolean check(String url,Integer ruleId){
		if(StringUtils.isBlank(url)){
			return true;
		}
		//缓存查询
		GoogleBloomFilter b = cache.get(ruleId);
		if(null == b){
			b = new GoogleBloomFilter();
			cache.put(ruleId,b);
		}
		return b.isDuplicate(url);
	}

	private Date getRandomDate(CrawlerRuleBean rule){
		Date viewDate = new Date();
		if(Boolean.valueOf(rule.getRuleBaseBean().getRandomDateFlag())){
			String format = StringUtils.isNotBlank(rule.getRuleBaseBean().getDateFormat()) ? rule.getRuleBaseBean().getDateFormat() : "yyyy-MM-dd";
			SimpleDateFormat dt = new SimpleDateFormat(format);
			SimpleDateFormat dft = new SimpleDateFormat("yyyy-MM-dd");
			String statrDate = StringUtils.isNotBlank(rule.getRuleBaseBean().getStartRandomDate()) ? rule.getRuleBaseBean().getStartRandomDate() : Constant.DEFAULT_START_DATE;
			String endDate = StringUtils.isNotBlank(rule.getRuleBaseBean().getEndRandomDate()) ? rule.getRuleBaseBean().getEndRandomDate() : dft.format(new java.util.Date());
			viewDate = DateFormatUtils.randomDate(statrDate, endDate);
			String tempDate = dt.format(viewDate);
			viewDate = DateUtil.strToDate(tempDate,format);
		}
		return viewDate;
	}

    
    
	public ICrawlerService<CrawlerTaskBean, CrawlerTaskCriteria> getCrawlerTaskService() {
		return crawlerTaskService;
	}
	public void setCrawlerTaskService(
			ICrawlerService<CrawlerTaskBean, CrawlerTaskCriteria> crawlerTaskService) {
		this.crawlerTaskService = crawlerTaskService;
	}
	public ICrawlerService<CrawlerRuleBean, CrawlerRuleCriteria> getCrawlerRuleService() {
		return crawlerRuleService;
	}
	public void setCrawlerRuleService(
			ICrawlerService<CrawlerRuleBean, CrawlerRuleCriteria> crawlerRuleService) {
		this.crawlerRuleService = crawlerRuleService;
	}
	public ICrawlerService<CrawlerContentBean, CrawlerContentCriteria> getCrawlerContentService() {
		return crawlerContentService;
	}
	public void setCrawlerContentService(
			ICrawlerService<CrawlerContentBean, CrawlerContentCriteria> crawlerContentService) {
		this.crawlerContentService = crawlerContentService;
	}
	public ICrawlerService<CrawlerContentCommentBean, CrawlerContentCommentCriteria> getCrawlerContentCommentService() {
		return crawlerContentCommentService;
	}
	public void setCrawlerContentCommentService(
			ICrawlerService<CrawlerContentCommentBean, CrawlerContentCommentCriteria> crawlerContentCommentService) {
		this.crawlerContentCommentService = crawlerContentCommentService;
	}
	public ICrawlerService<CrawlerContentPaginationBean, CrawlerContentPaginationCriteria> getCrawlerContentPaginationService() {
		return crawlerContentPaginationService;
	}
	public void setCrawlerContentPaginationService(
			ICrawlerService<CrawlerContentPaginationBean, CrawlerContentPaginationCriteria> crawlerContentPaginationService) {
		this.crawlerContentPaginationService = crawlerContentPaginationService;
	}
	public ICrawlerService<CrawlerContentResourceBean, CrawlerContentResourceCriteria> getCrawlerContentResourceService() {
		return this.crawlerContentResourceService;
	}
	public void setCrawlerContentResourceService(
			ICrawlerService<CrawlerContentResourceBean, CrawlerContentResourceCriteria> crawlerContentResourceService) {
		this.crawlerContentResourceService = crawlerContentResourceService;
	}
	
	public ICrawlerService<CrawlerExtendFieldBean, CrawlerExtendFieldCriteria> getCrawlerExtendFieldService() {
		return crawlerExtendFieldService;
	}
	public void setCrawlerExtendFieldService(
			ICrawlerService<CrawlerExtendFieldBean, CrawlerExtendFieldCriteria> crawlerExtendFieldService) {
		this.crawlerExtendFieldService = crawlerExtendFieldService;
	}
	public ICrawlerService<CrawlerHistoryBean, CrawlerHistoryCriteria> getCrawlerHistoryService() {
		return crawlerHistoryService;
	}
	public void setCrawlerHistoryService(
			ICrawlerService<CrawlerHistoryBean, CrawlerHistoryCriteria> crawlerHistoryService) {
		this.crawlerHistoryService = crawlerHistoryService;
	}
	public DBConnectionManager getConnectionManager() {
		if(null == connectionManager){
			connectionManager = DBConnectionManager.getInstance();
		}
		return connectionManager;
	}
	/* (non-Javadoc)
	 * @see org.javacoo.cowswing.core.event.CowSwingListener#update(org.javacoo.cowswing.core.event.CowSwingEvent)
	 */
	@Override
	public void update(CowSwingEvent event) {
		//采集完成，开启入库任务
		if (CowSwingEventType.TaskFinishedEvent == event.getEventType()) {
			Object obj = event.getEventObject();
			if(null != obj){
				//保存内容至远程数据库
				uploadContent(Integer.valueOf(obj.toString()));
			}
		}else if (CowSwingEventType.SaveFinishedEvent == event.getEventType()) {//入库完成，开启图片处理
			Object obj = event.getEventObject();
			if(null != obj){
				//处理资源
				dealWithResource(Integer.valueOf(obj.toString()));
			}
		}else if (CowSwingEventType.DealWithImageFinishedEvent == event.getEventType()) {//图片处理完成，开启FTP上传
			Object obj = event.getEventObject();
			if(null != obj){
				//上传资源
				uploadResource(Integer.valueOf(obj.toString()));
			}
		}else if (CowSwingEventType.FtpFinishedEvent == event.getEventType()) {//FTP上传完成，开启数据处理
			Object obj = event.getEventObject();
			if(null != obj){
				//数据处理
				dataHandle(Integer.valueOf(obj.toString()));
			}
		}
	}
	
	/**
	 * 删除缓存
	 * <p>方法说明:</>
	 * <li></li>
	 * @author DuanYong
	 * @since 2015-3-1 下午3:22:49
	 * @version 1.0
	 * @exception 
	 * @param ruleId
	 */
	private void removeUrlCache(Integer ruleId){
		log.error("===================删除URL缓存:"+ruleId);
		cache.remove(ruleId);
	}
	/**
	 * 根据规则ID，取得缓存中的规则
	 * <p>方法说明:</>
	 * <li></li>
	 * @author DuanYong
	 * @since 2015-3-3 下午6:55:13
	 * @version 1.0
	 * @exception 
	 * @param ruleId
	 * @return
	 */
	private CrawlerRuleBean getCrawlerRuleBeanFromCache(Integer ruleId){
		if(!crawlerRuleCache.containsKey(ruleId)){
			log.error("===================规则缓存中未缓存规则:"+ruleId+",从数据库获取并缓存");
			CrawlerRuleBean rule = new CrawlerRuleBean();
			rule.setRuleId(ruleId);
			rule = this.getCrawlerRuleService().get(rule, GatherConstant.SQLMAP_ID_GET_CRAWLER_RULE);
			crawlerRuleCache.put(ruleId, rule);
		}
		return crawlerRuleCache.get(ruleId);
	}
	/**
	 * 删除缓存中的规则
	 * <p>方法说明:</>
	 * <li></li>
	 * @author DuanYong
	 * @since 2015-3-3 下午7:04:06
	 * @version 1.0
	 * @exception 
	 * @param ruleId
	 */
	private void removeCrawlerRuleCache(Integer ruleId){
		crawlerRuleCache.remove(ruleId);
	}
	/**
	 * 根据规则ID，取得缓存中的任务
	 * <p>方法说明:</>
	 * <li></li>
	 * @author DuanYong
	 * @since 2015-3-3 下午8:47:38
	 * @version 1.0
	 * @exception 
	 * @param ruleId
	 * @param type
	 * @return
	 */
	private CrawlerTaskBean getCrawlerTaskBeanFromCache(Integer ruleId,String type){
		if(!crawlerTaskCache.containsKey(ruleId)){
			log.info("===================规则缓存中未缓存任务:"+ruleId+",从数据库获取并缓存");
			CrawlerTaskBean crawlerTaskBean = new CrawlerTaskBean();
			crawlerTaskBean.setRuleId(ruleId);
			crawlerTaskBean.setType(type);
			crawlerTaskBean = getCrawlerTaskService().get(crawlerTaskBean,GatherConstant.SQLMAP_ID_GET_BY_RULEID_CRAWLER_TASK);
			crawlerTaskCache.put(ruleId, crawlerTaskBean);
		}
		return crawlerTaskCache.get(ruleId);
	}
	/**
	 * 删除任务缓存
	 * <p>方法说明:</>
	 * <li></li>
	 * @author DuanYong
	 * @since 2015-3-3 下午8:50:47
	 * @version 1.0
	 * @exception 
	 * @param ruleId
	 */
	private void removeCrawlerTaskCache(Integer ruleId){
		crawlerTaskCache.remove(ruleId);
	}
	/**
	 * 删除相关缓存
	 * <p>方法说明:</>
	 * <li></li>
	 * @author DuanYong
	 * @since 2015-3-3 下午7:06:23
	 * @version 1.0
	 * @exception 
	 * @param ruleId
	 */
	private void removeCache(Integer ruleId,String isAuto){
		if(GatherConstant.TRUE_FALSE_FLAG_FALSE.equals(isAuto)){
			removeUrlCache(ruleId);
		}
		removeCrawlerRuleCache(ruleId);
		removeCrawlerTaskCache(ruleId);
	}
	private  void getKeywords(String content){ 
		List<String> keywordList = new ArrayList<String>();

		analyzer = new IKAnalyzer(true); 
		StringReader reader = new StringReader(content);
	    TokenStream ts;
	    System.out.println("content======"+content);
		try { 
			ts = analyzer.tokenStream("", reader);
			CharTermAttribute term = ts.getAttribute(CharTermAttribute.class);  
			System.out.println("");
			//遍历分词数据  
		    while(ts.incrementToken()){ 
		    	if(term.toString().length() > 1
		    			//&& !keywordList.contains(tempToken)
		    			){
		    		 System.out.print(term.toString()+"|");   
		    	}
		    } 
		    System.out.println("");
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			reader.close();  
		}
	}
	public static void main(String[] args) throws IOException {  
        String text="基于java语言开发的轻量级的中文分词工具包";  
        //创建分词对象  
        Analyzer anal = new IKAnalyzer(true);       
        StringReader reader=new StringReader(text);  
        //分词  
        //anal.tokenStream("", reader);
        TokenStream ts= anal.tokenStream("", reader);  
        CharTermAttribute term=ts.getAttribute(CharTermAttribute.class);  
        //遍历分词数据  
        while(ts.incrementToken()){  
            System.out.print(term.toString()+"|");  
        }  
        reader.close();  
        System.out.println("");
        InputStream ip = new ByteArrayInputStream(text.getBytes());  
        Reader read = new InputStreamReader(ip);  
        IKSegmenter iks = new IKSegmenter(read,true);//true开启只能分词模式，如果不设置默认为false，也就是细粒度分割  
        Lexeme t;  
        while ((t = iks.next()) != null) {  
        	System.out.print(t.getLexemeText()+"|"); 
        	
        }  
    }  
}

